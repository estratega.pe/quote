<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class calendarModel extends MX_Controller {
	
	const CALENDAR = 'jyc_quote';
	const COMPANY = 'jyc_company';
	const LOCAL = 'jyc_local';
	const R_C_E = 'rel_company_employee';
	const R_C_U = 'rel_company_user';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getEvents($company_id, $local_id, $where) {
    	$this->db->select('*');
    	$this->db->from(self::CALENDAR);
    	$this->db->where('company_id', $company_id);
    	$this->db->where('local_id', $local_id);
    	$this->db->where($where);
    	return$this->db->get()->result();
    }
    
    function getRowEvent($event) {
    	$this->db->select('*');
    	$this->db->from(self::CALENDAR);
    	$this->db->join(self::COMPANY, self::COMPANY . '.company_id=' . self::CALENDAR . '.company_id');
    	$this->db->join(self::LOCAL, self::LOCAL . '.local_id=' . self::CALENDAR . '.local_id');
    	if($this->session->userdata('USER')->user_type == $this->config->item('IND_ADMIN')) {
    		$this->db->join(self::R_C_U, self::R_C_U . '.company_id=' . self::CALENDAR . '.company_id');
    		$this->db->where([self::R_C_U . '.user_id' => $this->session->userdata('USER')->user_id
    				,self::R_C_U . '.user_type'	=> $this->config->item('IND_ADMIN')
    		]);
    	} else {
    		$this->db->join(self::R_C_E, self::R_C_E . '.company_id=' . self::CALENDAR . '.company_id');
    		$this->db->where([self::R_C_E . '.user_id' => $this->session->userdata('USER')->user_id
    				,self::R_C_E . '.user_type'	=> $this->config->item('IND_EMPLOYEE')
    		]);
    	}
    	$this->db->where(self::CALENDAR . '.quote_id', $event);
    	return $this->db->get()->row();
    }
}
