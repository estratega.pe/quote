<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>{BODY_TITLE}</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
	<link href="{BASE_URL}assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{BASE_URL}assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="{BASE_URL}assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link href="{BASE_URL}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="{BASE_URL}css/style.css" rel="stylesheet" />
    <link href="{BASE_URL}css/style-responsive.css" rel="stylesheet" />
    <link href="{BASE_URL}css/style-default.css" rel="stylesheet" id="style_color" />

    <link href="{BASE_URL}assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{BASE_URL}assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="lock">
    <div class="lock-header">
        <!-- BEGIN LOGO -->
        <a class="center" id="logo" href="{BASE_URL}">
            <img class="center" alt="logo" src="{BASE_URL}img/logo-casistemas.jpg">
        </a>
        <!-- END LOGO -->
    </div>
        
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget box {BG_COLOR}">
                        <div class="widget-title">
                            <h4>
                                <i class="icon-reorder"></i> {BODY_SUBTITLE}</span>
                            </h4>
                        <span class="tools">

                        </span>
                        </div>
                        <div class="widget-body">
                            <form id="firstSetup" class="form-horizontal" method="post" action="{URL_POST}">
                                <div id="tabsleft" class="tabbable tabs-left">
                                <ul>
                                    <li><a href="#tabsleft-tab1" data-toggle="tab"><span class="strong">Paso 1</span> <span class="muted">Detalles de la empresa</span></a></li>
                                    <!-- <li><a href="#tabsleft-tab2" data-toggle="tab"><span class="strong">Paso 2</span> <span class="muted"></span></a></a></li>
                                    <li><a href="#tabsleft-tab3" data-toggle="tab"><span class="strong">Paso 3</span> <span class="muted">Payment Details</span></a></a></li>
                                    <li><a href="#tabsleft-tab4" data-toggle="tab"><span class="strong">Paso 4</span> <span class="muted">Confirmation</span></a></a></li>-->
                                </ul>
                                <div class="progress progress-info progress-striped">
                                    <div class="bar"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane" id="tabsleft-tab1">
                                        <h3>Rellene los datos de su empresa</h3>
                                        <div class="control-group">
                                            <label class="control-label">Raz&oacute;n Social</label>
                                            <div class="controls">
                                                <input type="text" name="company_social" class="span6" required>
                                                <span class="help-inline">Ingrese el nombre de la Raz&oacute;n Social. (* obligatorio)</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Nombre Comercial</label>
                                            <div class="controls">
                                                <input type="text" name="company_comercial" class="span6" required>
                                                <span class="help-inline">Ingrese el nombre comercial de la empresa.</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">R.U.C.</label>
                                            <div class="controls">
                                                <input type="number" min="10000000000" max="20999999999" name="company_ruc" class="span6" required>
                                                <span class="help-inline">Ingrese el R.U.C. (* obligatorio)</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <div class="controls">
                                                <input type="email" name="company_email" class="span6" required>
                                                <span class="help-inline">Email de la empresa. (* obligatorio)</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Tel&eacute;fono</label>
                                            <div class="controls">
                                                <input type="number" name="company_phone" class="span6">
                                                <span class="help-inline">Tel&eacute;fono de la empresa</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Direcci&oacute;n</label>
                                            <div class="controls">
                                                <textarea name="company_address" cols="6" rows="6" class="span6" required></textarea>
                                                <span class="help-inline">Direcci&oacute;n de la empresa. (* obligatorio)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabsleft-tab2">
                                        <h3>Fill up step 2</h3>
                                        <div class="control-group">
                                            <label class="control-label">Billing Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6">
                                                <span class="help-inline">Give your Billing Name</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Billing Details</label>
                                            <div class="controls">
                                                <input type="password" class="span6">
                                                <span class="help-inline">Give your Billing Details</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabsleft-tab3">
                                        <h3>Fill up step 3</h3>
                                        <div class="control-group">
                                            <label class="control-label">Text Input</label>
                                            <div class="controls">
                                                <input type="text" class="span6" />
                                                <span class="help-inline"></span>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Checkbox and radio Options</label>
                                            <div class="controls">
                                                <label class="checkbox line">
                                                    <input type="checkbox" value="" /> Lorem ipsum dolor imti
                                                </label>
                                                <label class="radio line">
                                                    <input type="radio" value="" /> Duis autem vel eum iriure dolor in hendrerit
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabsleft-tab4">
                                        <h3>Final step</h3>
                                        <div class="control-group">
                                            <label class="control-label">Fullname:</label>
                                            <div class="controls">
                                                <span class="text">Jhon Doe </span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Email:</label>
                                            <div class="controls">
                                                <span class="text">dkmosa@gmail.com</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Phone:</label>
                                            <div class="controls">
                                                <span class="text">123456789</span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"></label>
                                            <div class="controls">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="" /> I confirm my steps
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="pager wizard">
                                        <li class="previous first"><a href="javascript:;">Primera</a></li>
                                        <li class="previous"><a href="javascript:;">Anterior</a></li>
                                        <li class="next last"><a href="javascript:;">&Uacute;ltima</a></li>
                                        <li class="next"><a href="javascript:;">Siguiente</a></li>
                                        <li class="next finish" style="display:none;"><a href="javascript:;">Terminar</a></li>
                                    </ul>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
          </div>
<!-- BEGIN FOOTER -->
   <div id="footer">
       <!-- 2016 - <?php //echo date('Y');?>&copy; www.jyconsultores.com -->
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="{BASE_URL}js/jquery-1.8.3.min.js"></script>
   <script src="{BASE_URL}js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="{BASE_URL}assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="{BASE_URL}assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="{BASE_URL}js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="{BASE_URL}js/excanvas.js"></script>
   <script src="{BASE_URL}js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="{BASE_URL}assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="{BASE_URL}assets/uniform/jquery.uniform.min.js"></script>
   <script src="{BASE_URL}js/jquery.scrollTo.min.js"></script>

   <!--common script for all pages-->
   <script src="{BASE_URL}js/common-scripts.js"></script>
   <!--script for this page-->
   <script>
   var Script = function () {
	   $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
	        console.log('onShow');
	    }, onNext: function(tab, navigation, index) {
	        console.log('onNext');
	    }, onPrevious: function(tab, navigation, index) {
	        console.log('onPrevious');
	    }, onLast: function(tab, navigation, index) {
	        console.log('onLast');
	    }, onTabClick: function(tab, navigation, index) {
	        console.log('onTabClick');

	    }, onTabShow: function(tab, navigation, index) {
	        console.log('onTabShow');
	        var $total = navigation.find('li').length;
	        var $current = index+1;
	        var $percent = ($current/$total) * 100;
	        $('#tabsleft').find('.bar').css({width:$percent+'%'});

	        // If it's the last tab then hide the last button and show the finish instead
	        if($current >= $total) {
	            $('#tabsleft').find('.pager .next').hide();
	            $('#tabsleft').find('.pager .finish').show();
	            $('#tabsleft').find('.pager .finish').removeClass('disabled');
	        } else {
	            $('#tabsleft').find('.pager .next').show();
	            $('#tabsleft').find('.pager .finish').hide();
	        }

	    }});

	   $('#tabsleft .finish').click(function() {
	        $('#firstSetup').submit();
	        //$('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
	    });
   }();
   </script>


   <!-- END JAVASCRIPTS -->
   <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>        		
</body>
<!-- END BODY -->
</html>