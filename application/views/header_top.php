<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user_photo = (file_exists($this->config->item('FOLDER_UPLOAD') . 'users/' . $this->session->userdata('USER')->user_id) ) ? base_url() . 'assets/uploads/users/' . $this->session->userdata('USER')->user_id . '_thumb' : base_url() . 'img/avatar.png';
?><!-- BEGIN HEADER -->
<div id="header" class="navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<!--BEGIN SIDEBAR TOGGLE-->
			<div class="sidebar-toggle-box hidden-phone">
				<div class="icon-reorder tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
			</div>
			<!--END SIDEBAR TOGGLE-->
			<!-- BEGIN LOGO -->
			<a class="brand" href="{BASE_URL}">
				<img src="{BASE_URL}img/logo-casistemas.jpg" alt="Metro Lab" />
			</a>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<div id="top_menu" class="nav notify-row">
			
			</div>
			<!-- END  NOTIFICATION -->
			<div class="top-nav ">
				<ul class="nav pull-right top-menu" >
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo $user_photo;?>" alt="">
							<span class="username">{USER_NAME}</span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu extended logout">
							<li><a href="{BASE_URL}user"><i class="icon-user"></i> Mi Cuenta</a></li>
							<li><a href="{BASE_URL}login/loginOut"><i class="icon-key"></i> Desconectar</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			<!-- END TOP NAVIGATION MENU -->
			</div>
		</div>
	</div>
<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
