<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('employee/employeeModel', 'employee');
		$this->load->model('company/companyModel', 'company');
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
	}
	
	public function index()
	{
		$c = $this->gen->getRow('gen_user', ['user_id' => $this->UID]);
		$subtitle = 'Editar Perfil';
		$subtitle_photo = 'Foto de Perfil';
		$subtitle_passwd = 'Cambiar contrase&ntilde;a';
		$bg_color = 'blue';
		$bg_color_photo = 'blue';
		$bg_color_passwd = 'blue';
		$user_firstname = $c->user_firstname;
		$user_lastname = $c->user_lastname;
		$user_email = $c->user_email;
		$user_status = $c->user_status;
		$user_type = $c->user_type;
		$company_id = $c->company_id;
		$local_id = $c->local_id;
		$user_fec_start_t = !empty($c->user_fec_start_t) ? converDate($c->user_fec_start_t) : '';
		$user_fec_end_t = !empty($c->user_fec_end_t) ? converDate($c->user_fec_end_t) : '';
		$user_fec_nac = !empty($c->user_fec_nac) ? converDate($c->user_fec_nac) : '';
		$user_doc_type = $c->user_doc_type;
		$user_doc_number = $c->user_doc_number;
		$user_sex = $c->user_sex;
		$method = $this->uri->segment(3);
		$user_photo = (file_exists($this->config->item('FOLDER_UPLOAD') . 'users/' . $this->UID) ) ? base_url() . 'assets/uploads/users/' . $this->UID : base_url() . 'assets/uploads/no-image.png';
		if($_FILES) {
			$check = getimagesize($_FILES["user_photo"]["tmp_name"]);
			$path = $this->config->item('FOLDER_UPLOAD') . 'users/';
			$photo = $path . $this->UID;
			if($check !== false) {
				if( file_exists($photo) ) {
					unlink($photo);
				}
				if( !move_uploaded_file($_FILES["user_photo"]["tmp_name"], $photo) ) {
					$subtitle_photo = 'Error al subir la imagen, por favor intentelo mas tarde.';
					$bg_color_photo = 'red';
				} else {
					createThumb($photo, 29, 29);
					$subtitle_photo = 'Imagen actualizada con &Eacute;xito.';
					$bg_color_photo = 'green';
					$user_photo = (file_exists($this->config->item('FOLDER_UPLOAD') . 'users/' . $this->UID) ) ? base_url() . 'assets/uploads/users/' . $this->UID : base_url() . 'assets/uploads/no-image.png';
				}
			} else {
				$subtitle_photo = 'El archivo no es un tipo de imagen v&aacute:lido.';
				$bg_color_photo = 'red';
			}
		}
		if($_POST) {
			switch ($method) {
				case 'userPhoto':
					
					break;
				case 'userPasswd':
					$user_passwd = $this->input->post('input_user_passwd', TRUE);
					$_salt = Modules::run('login/createSalt');
					$_passwd = crypt($user_passwd, $_salt);
					$data = [
							'user_passwd'	=> $_passwd
							,'user_salt'	=> $_salt
					];
					if( !$this->gen->updateData('gen_user', $data, ['user_id' => $this->UID]) ) {
						$subtitle_passwd = 'No se ha podido actualizar la Contrase&ntilde;a, por favor intentelo mas tarde.';
						$bg_color_passwd = 'red';
					} else {
						$subtitle_passwd = 'Contrase&ntilde;a actualizada correctamente.';
						$bg_color_passwd = 'green';
					}
					break;
				default:
					$user_firstname = $this->input->post('input_user_firstname', TRUE);
					$user_lastname = $this->input->post('input_user_lastname', TRUE);
					$user_sex = $this->input->post('select_user_sex');
					$user_doc_type = $this->input->post('select_user_doc_type');
					$user_doc_number = $this->input->post('input_user_doc_number');
					$user_fec_nac = $this->input->post('input_user_fec_nac', TRUE);
					$data = [
							'user_firstname'	=> $user_firstname
							,'user_lastname'	=> $user_lastname
							,'user_sex'			=> $user_sex
							,'user_doc_type'	=> $user_doc_type
							,'user_doc_number'	=> $user_doc_number
					];
					if( !empty($user_fec_nac) )
						$data['user_fec_nac'] = $user_fec_nac;
					if( !$this->gen->updateData('gen_user', $data, ['user_id' => $this->UID]) ) {
						$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
						$bg_color = 'red';
					} else {
						$subtitle = 'Perfil actualizado correctamente.';
						$bg_color = 'green';
					}
					break;
			}
		}
		
		$select_doc_id = [];
		$d = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_DOC')]);
		foreach($d as $k) {
			$select_doc_id[$k->select_id] = $k->select_name;
		}
		$select_user_sex = ['' => 'Seleccione'];
		$s = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_SEX')]);
		foreach($s as $k) {
			$select_user_sex[$k->select_id] = $k->select_name;
		}
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Perfil', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Editar Perfil';
		$data['BODY'] = $this->parser->parse('user/editUser', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Editar Perfil'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_SUBTITLE_PHOTO'		=> $subtitle_photo
				,'BODY_SUBTITLE_PASSWD'		=> $subtitle_passwd
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'BG_COLOR_PHOTO'			=> $bg_color_photo
				,'BG_COLOR_PASSWD'			=> $bg_color_passwd
				,'USER_PHOTO'				=> $user_photo
				,'URL_POST'					=> base_url() .'user/index/'
				,'URL_POST_PHOTO'			=> base_url() .'user/index/userPhoto/'
				,'URL_POST_PASSWD'			=> base_url() .'user/index/userPasswd/'
				,'INPUT_USER_FIRSTNAME'		=> form_input(array('name' => 'input_user_firstname', 'id' => 'input_user_firstname', 'maxlength' => '120'), $user_firstname, 'class="span6" required ')
				,'INPUT_USER_LASTNAME'		=> form_input(array('name' => 'input_user_lastname', 'id' => 'input_user_lastname', 'maxlength' => '120'), $user_lastname, 'class="span6" ')
				,'INPUT_USER_EMAIL'			=> form_input(array('name' => 'input_user_email', 'id' => 'input_user_email', 'maxlength' => '100'), $user_email, 'class="span6" readonly')
				,'INPUT_USER_LOGIN'			=> form_input(array('name' => 'input_user_login', 'id' => 'input_user_login', 'maxlength' => '100'), $user_email, 'class="span6" readonly')
				,'SELECT_USER_SEX'			=> form_dropdown(array('name' => 'select_user_sex', 'id' => 'select_user_sex'), $select_user_sex, $user_sex, 'class="span6" required required')
				,'SELECT_USER_DOC_TYPE'		=> form_dropdown(array('name' => 'select_user_doc_type', 'id' => 'select_user_doc_type'), $select_doc_id, $user_doc_type, 'class="span6" required ')
				,'INPUT_USER_DOC_NUMBER'	=> form_input(array('name' => 'input_user_doc_number', 'id' => 'input_user_doc_number', 'maxlength' => '30'), $user_doc_number, 'class="span6" required ')
				,'INPUT_USER_FEC_START_T'	=> form_input(array('name' => 'input_user_fec_start_t', 'id' => 'input_user_fec_start_t', 'size' => '16'), $user_fec_start_t, 'class="m-ctrl-medium" readonly ')
				,'INPUT_USER_FEC_END_T'		=> form_input(array('name' => 'input_user_fec_end_t', 'id' => 'input_user_fec_end_t', 'size' => '16'), $user_fec_end_t, 'class="m-ctrl-medium" readonly ')
				,'INPUT_USER_FEC_NAC'		=> form_input(array('name' => 'input_user_fec_nac', 'id' => 'input_user_fec_nac', 'size' => '16'), $user_fec_nac, 'class="m-ctrl-medium" readonly ')
				,'INPUT_USER_PASSWD'		=> form_input(array('name' => 'input_user_passwd', 'id' => 'input_user_passwd', 'maxlength' => '120'), '', 'class="span6" ')
				//,'TEXTAREA_DES_DIRECCION'	=> form_textarea(array('name' => 'textarea_des_direccion', 'id' => 'textarea_des_direccion', 'maxlength' => '300'), nl2br($des_direccion), 'class="span6" required ')
				//,'SELECT_COMPANY_ID'		=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-success', 'icon-save')
				,'BUTTON_SUBMIT_PHOTO'		=> createSubmitButton('Guardar', 'btn-success', 'icon-save')
				,'BUTTON_SUBMIT_PASSWD'		=> createSubmitButton('Cambiar', 'btn-success', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . '', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$data['FOOTER_EXTRA'] = "<script>var Script = function () {
		if (top.location != location) {
		top.location.href = document.location.href ;
		}
		$(function(){
		$('#dpYears').datepicker();
		$('#dpYearsE').datepicker();
		$('#dpNac').datepicker();
		});
		}();</script>";
		renderPage($data);
	}
	
}
