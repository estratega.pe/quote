<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>Registro</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="{BASE_URL}assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="{BASE_URL}assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="{BASE_URL}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="{BASE_URL}css/style.css" rel="stylesheet" />
   <link href="{BASE_URL}css/style-responsive.css" rel="stylesheet" />
   <link href="{BASE_URL}css/style-default.css" rel="stylesheet" id="style_color" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="lock">
    <div class="lock-header">
        <!-- BEGIN LOGO -->
        <a class="center" id="logo" href="{BASE_URL}">
            <img class="center" alt="logo" src="{BASE_URL}img/logo-casistemas.jpg">
        </a>
        <!-- END LOGO -->
    </div>
    <div class="login-wrap">
        
        
        			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                            <form class="cmxform form-horizontal" id="signupForm" method="post" action="{URL_POST}">
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Nombres</label>
                                    <div class="controls">
                                        {INPUT_USER_FIRSTNAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Apellidos</label>
                                    <div class="controls">
                                        {INPUT_USER_LASTNAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">E-mail</label>
                                    <div class="controls">
                                        {INPUT_USER_EMAIL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Contrase&ntilde;a.</label>
                                    <div class="controls">
                                        {INPUT_USER_PASSWD}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Sexo</label>
                                    <div class="controls">
                                        {SELECT_USER_SEX}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Tipo Doc.</label>
                                    <div class="controls">
                                        {SELECT_USER_DOC_TYPE}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">N&uacute;mero Doc.</label>
                                    <div class="controls">
                                        {INPUT_USER_DOC_NUMBER}
                                    </div>
                                </div>
                                <!-- 
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">T&eacute;rminos y Condiciones.</label>
                                    <div class="controls">
                                        {TEXTAREA_TERM_COND}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Acepto los t&eacute;rminos y condiciones.</label>
                                    <div class="controls">
                                        {CHECK_TERM_COND}
                                    </div>
                                </div>
                                -->
                                
                                <div class="form-actions">
                                    {BUTTON_SUBMIT}
                                    {BUTTON_CANCEL}
                                </div>

                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
			</div>
</body>
<!-- END BODY -->
</html>