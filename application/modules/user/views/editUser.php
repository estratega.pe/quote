<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span6">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                            <form class="cmxform form-horizontal" id="signupForm" method="post" action="{URL_POST}">
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Nombres</label>
                                    <div class="controls">
                                        {INPUT_USER_FIRSTNAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Apellidos</label>
                                    <div class="controls">
                                        {INPUT_USER_LASTNAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">E-mail</label>
                                    <div class="controls">
                                        {INPUT_USER_EMAIL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Sexo</label>
                                    <div class="controls">
                                        {SELECT_USER_SEX}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Tipo Doc.</label>
                                    <div class="controls">
                                        {SELECT_USER_DOC_TYPE}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">N&uacute;mero Doc.</label>
                                    <div class="controls">
                                        {INPUT_USER_DOC_NUMBER}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Fecha de Nacimiento</label>
                                    <div class="controls">
                                    	<div class="input-append date" id="dpNac" data-date="11-11-2016"
                                             data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                            {INPUT_USER_FEC_NAC}
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    {BUTTON_SUBMIT}
                                    {BUTTON_CANCEL}
                                </div>

                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
                <div class="span6">
                	<div class="widget {BG_COLOR_PHOTO}">
                		<div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE_PHOTO}</h4>
                        </div>
                		<div class="widget-body form ">
	                        <form class="cmxform form-horizontal" id="signupForm" method="post" action="{URL_POST_PHOTO}" enctype="multipart/form-data">
	                        <div class="control-group">
								<label class="control-label">Foto actual</label>
								<div class="controls">
									<div data-provides="fileupload" class="fileupload fileupload-new">
										<div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
											<img alt="" src="{USER_PHOTO}">
										</div>
										<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
										<div>
											<span class="btn btn-file"><span class="fileupload-new">Seleccionar imagen</span>
											<span class="fileupload-exists">Cambiar</span>
											<input name="user_photo" type="file" class="default"></span>
											<a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Eliminar</a>
										</div>
									</div>
								</div>
							</div>
							<div class="form-actions">
								{BUTTON_SUBMIT_PHOTO}
								{BUTTON_CANCEL}
							</div>
							</form>
						</div>
					</div>
					<div class="widget {BG_COLOR_PASSWD}">
						<div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE_PASSWD}</h4>
                        </div>
                        <div class="widget-body form ">
                        	<form class="cmxform form-horizontal" id="signupForm" method="post" action="{URL_POST_PASSWD}">
                        	<div class="control-group ">
                        		<label for="firstname" class="control-label">Login</label>
                        		<div class="controls">
                        			{INPUT_USER_LOGIN}
                        		</div>
                        	</div>
	                		<div class="control-group ">
								<label for="lastname" class="control-label">Contrase&ntilde;a</label>
								<div class="controlse">
									{INPUT_USER_PASSWD}
								</div>
							</div>
							<div class="form-actions">
								{BUTTON_SUBMIT_PASSWD}
								{BUTTON_CANCEL}
							</div>
							</form>
	                	</div>
                	</div>
                </div>
			</div>