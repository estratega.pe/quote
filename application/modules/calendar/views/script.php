<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>var Script = function () {
	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        droppable: false,
        events: {
	        url: '{BASE_URL}calendar/getEvents/{COMPANY_ID}/{LOCAL_ID}/',
	        type: 'POST',
	        data: {
	            company_id: '{COMPANY_ID}',
	            local_id: '{LOCAL_ID}'
	        },
	        error: function() {
	            alert('there was an error while fetching events!');
	        },
	        color: 'yellow',
	        textColor: 'black'
	    }
    });
    
}();