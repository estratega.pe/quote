<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
   <script src="{BASE_URL}js/jquery-1.8.3.min.js"></script>
   <script src="{BASE_URL}js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="{BASE_URL}assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="{BASE_URL}js/jquery.blockui.js"></script>

   <script type="text/javascript" src="{BASE_URL}assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="{BASE_URL}assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="{BASE_URL}assets/data-tables/DT_bootstrap.js"></script>
   <script src="{BASE_URL}js/jquery.scrollTo.min.js"></script>
   



<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel-B">
	<div class="panel-B-body">
		<h3 class="title-hero">
		{BODY_DESCRIPTION}
		</h3>
		<div class="example-box-wrapper">
			<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
			<thead>
				<tr>
				{TH_TABLE}
					<th>{LABEL}</th>
				{/TH_TABLE}
				</tr>
			</thead>
			
			<tfoot>
				<tr>
				{TH_TABLE}
				    <th>{LABEL}</th>
				{/TH_TABLE}
				</tr>
			</tfoot>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">

    /* Datatables responsive */
    var Script = function () {
        $('#datatable-responsive').dataTable( {
            responsive: true
            ,bProcessing: true
            ,sAjaxSource:"{URL_AJAX}"
            ,sServerMethod: "POST"
            ,bServerSide: true
            //,bStateSave: true
            ,oLanguage: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "&Uacute;ltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        	,autoWidth: false
        	,fixedColumns: true
		});

        $('.dataTables_filter input').attr("placeholder", "Buscar...");
    }();

    //$(document).ready(function() {
    //    $('.dataTables_filter input').attr("placeholder", "Buscar...");
    //});

</script>