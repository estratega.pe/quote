<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class homeModel extends MX_Controller {
	
	const USUARIO = 'gen_user';
     
    function __construct()
    {
        parent::__construct();
    }
     
    function loginIn($where)
    {
        $query = $this->db->get_where(self::USUARIO, $where)->result();
        return $query;
    }	
}
