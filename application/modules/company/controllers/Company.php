<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MX_Controller {
	public $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('company/companyModel', 'company');
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
	}
	
	public function index()
	{
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'RAZON SOCIAL']
				,['LABEL' => 'RUC']
				,['LABEL' => 'ACCIONES']
		];
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Empresas', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Empresas'
			,'URL_AJAX'			=> base_url() . 'company/getCompany/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Empresas'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'company_id'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> createLink(base_url() . 'company/addCompany', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'TH_TABLE'			=> $columnas
		], TRUE);
		renderPage($data);
	}
	
	public function addCompany() {
		$subtitle = 'Crear nueva empresa';
		$bg_color = 'blue';
		$company_social = '';
		$company_comercial = '';
		$company_ruc = '';
		$company_direccion = '';
		$company_id = '';
		$company_email = '';
		$company_phone = '';
		if( $this->input->post() ) {
			$company_social = $this->input->post('input_company_social', TRUE);
			$company_comercial = $this->input->post('input_company_comercial', TRUE);
			$company_ruc = $this->input->post('input_company_ruc');
			$company_direccion = $this->input->post('textarea_company_direccion', TRUE);
			$company_email = $this->input->post('input_company_email', TRUE);
			$company_phone = $this->input->post('input_company_phone', TRUE);
			if( empty($company_social) || empty($company_ruc) || empty($company_direccion) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'company_social'		=> $company_social
						,'company_comercial'	=> $company_comercial
						,'company_address'		=> $company_direccion
						,'company_ruc'			=> $company_ruc
						,'company_email'		=> $company_email
						,'company_phone'		=> $company_phone
				];
				$newID = $this->company->saveCompany($data, ['user_id' => $this->UID, 'user_type' => $this->session->userdata('USER')->user_type]);
				if(  !is_numeric($newID) ) {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'company/viewCompany/' . $newID);
				}
			}
			
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nueva Empresa', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Nueva Empresa';
		$data['BODY'] = $this->parser->parse('company/addCompany', ['BASE_URL' => base_url()
			,'BODY_TITLE'					=> 'Nueva Empresa'
			,'BODY_SUBTITLE'				=> $subtitle
			,'BODY_MENU'					=> ''
			,'BG_COLOR'						=> $bg_color
			,'URL_POST'						=> base_url() .'company/addCompany/'
			,'INPUT_COMPANY_SOCIAL'			=> form_input(array('name' => 'input_company_social', 'id' => 'input_company_social', 'maxlength' => '150'), $company_social, 'class="span6" required ')
			,'INPUT_COMPANY_COMERCIAL'		=> form_input(array('name' => 'input_company_comercial', 'id' => 'input_company_comercial', 'maxlength' => '150'), $company_comercial, 'class="span6" ')
			,'INPUT_COMPANY_RUC'			=> form_input(array('name' => 'input_company_ruc', 'type' => 'number', 'id' => 'input_company_ruc', 'min' => '10000000000', 'max' => '20999999999'), $company_ruc, 'class="span6" required' )
			,'INPUT_COMPANY_EMAIL'			=> form_input(array('name' => 'input_company_email', 'type' => 'email', 'id' => 'input_company_email', ), $company_email, 'class="span6" ' )
			,'INPUT_COMPANY_PHONE'			=> form_input(array('name' => 'input_company_phone', 'type' => 'number', 'id' => 'input_company_phone', 'min' => '1000000', 'max' => '999999999'), $company_phone, 'class="span6" ' )
			,'TEXTAREA_COMPANY_DIRECCION'	=> form_textarea(array('name' => 'textarea_company_direccion', 'id' => 'textarea_company_direccion', 'maxlength' => '300'), nl2br($company_direccion), 'class="span6" required ')
			,'BUTTON_SUBMIT'				=> createSubmitButton('Crear', 'btn-success', 'icon-save')
			,'BUTTON_CANCEL'				=> createLink(base_url() . 'company', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editCompany() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'company');
			exit();
		}
		$subtitle = 'Editar empresa';
		$bg_color = 'blue';
		$c = $this->company->getRowCompany(['rel_company_user.company_id' => $ID, 'user_type' => $this->UTYPE, 'user_id' => $this->UID]);
		if(count($c) <= 0x0000) {
			redirect(base_url() . 'company/');
		}
		if( $this->input->post() ) {
			$company_social = $this->input->post('input_company_social', TRUE);
			$company_comercial = $this->input->post('input_company_comercial', TRUE);
			$company_address = $this->input->post('textarea_company_address', TRUE);
			$company_email = $this->input->post('input_company_email', TRUE);
			$company_phone = $this->input->post('input_company_phone', TRUE);
			if( empty($company_social) || empty($company_address) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'company_social'		=> $company_social
						,'company_comercial'	=> $company_comercial
						,'company_address'		=> $company_address
						,'company_email'		=> $company_email
						,'company_phone'		=> $company_phone
				];
				$where = ['company_id' => $ID];
				if(  !$this->gen->updateData('jyc_company', $data, $where) ) {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'company/viewCompany/' . $ID);
				}
			}
				
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Empresa', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Editar Empresa';
		$data['BODY'] = $this->parser->parse('company/editCompany', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Editar Empresa'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'URL_POST'					=> base_url() .'company/editCompany/' . $ID
				,'INPUT_COMPANY_SOCIAL'		=> form_input(array('name' => 'input_company_social', 'id' => 'input_company_social', 'maxlength' => '150'), $c->company_social, 'class="span6" required ')
				,'INPUT_COMPANY_COMERCIAL'	=> form_input(array('name' => 'input_company_comercial', 'id' => 'input_company_comercial', 'maxlength' => '150'), $c->company_comercial, 'class="span6" ')
				,'INPUT_COMPANY_RUC'		=> form_input(array('name' => 'input_company_ruc', 'type' => 'number', 'id' => 'input_company_ruc', 'min' => '10000000000', 'max' => '20999999999'), $c->company_ruc, 'class="span6" readonly' )
				,'INPUT_COMPANY_EMAIL'		=> form_input(array('name' => 'input_company_email', 'type' => 'email', 'id' => 'input_company_email', ), $c->company_email, 'class="span6" ' )
				,'INPUT_COMPANY_PHONE'		=> form_input(array('name' => 'input_company_phone', 'type' => 'number', 'id' => 'input_company_phone', 'min' => '1000000', 'max' => '999999999'), $c->company_phone, 'class="span6" ' )
				,'TEXTAREA_COMPANY_ADDRESS'	=> form_textarea(array('name' => 'textarea_company_address', 'id' => 'textarea_company_address', 'maxlength' => '300'), nl2br($c->company_address), 'class="span6" required ')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-success', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'company', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewCompany() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'company');
			exit();
		}
		$subtitle = 'Ver Empresa';
		$bg_color = 'blue';
		$c = $this->company->getRowCompany(['rel_company_user.company_id' => $ID, 'user_type' => $this->UTYPE, 'user_id' => $this->UID]);
		if(count($c) <= 0x0000) {
			redirect(base_url() . 'company/');
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Empresa', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Ver Empresa';
		$data['BODY'] = $this->parser->parse('company/viewCompany', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Ver Empresa'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'INPUT_DES_SOCIAL'			=> $c->company_social
				,'INPUT_DES_COMERCIAL'		=> $c->company_comercial
				,'INPUT_DES_RUC'			=> $c->company_ruc
				,'INPUT_COMPANY_EMAIL'		=> $c->company_email
				,'INPUT_COMPANY_PHONE'		=> $c->company_phone
				,'TEXTAREA_DES_DIRECCION'	=> nl2br($c->company_address)
				,'LINK_BACK'				=> createLink(base_url() . 'company', ' ', 'icon-ban', 'regresar al listado', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteCompany() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'company');
			exit();
		}
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n';
		$c = $this->company->getRowCompany(['rel_company_user.company_id' => $ID, 'user_type' => $this->UTYPE, 'user_id' => $this->UID]);
		if(count($c) <= 0x0000) {
			redirect(base_url() . 'company/');
		}
		if($this->input->post()) {
			if( $this->input->post('input_company_id') == $ID) {
				$data = [
						'company_status'	=> 0
				];
				$where = [
						'company_id'	=> $this->input->post('input_company_id')
				];
				if (!$this->gen->updateData('jyc_company', $data, $where) ) {
					$this->db->trans_rollback();
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'company');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Empresa ' . $c->company_social, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Empresa - ' . $c->company_social
			,'BODY_SUBTITLE'			=> $subtitle
			,'BODY_MENU'				=> '(' . $c->company_ruc . ') ' . $c->company_social
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion de la Empresa "' . $c->company_social . '"'
			,'BG_COLOR'					=> $bg_color
			,'URL_POST'					=> base_url() . 'company/deleteCompany/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_company_id', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'company', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function recoveryCompany() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'company');
			exit();
		}
		$c = $this->company->getRowCompany(['rel_company_user.company_id' => $ID, 'user_type' => $this->UTYPE, 'user_id' => $this->UID]);
		if(count($c) == 0x0001 && ($c->company_id == $ID) ) {
			$data = [
					'company_status'	=> 1
			];
			$where = [
					'company_id'	=> $ID
			];
			if (!$this->gen->updateData('jyc_company', $data, $where) ) {
				$this->db->trans_rollback();
			} else {
					
			}
		}
		
		redirect(base_url() . 'company');
	}
	
	public function getCompany() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$where = [
				'user_id'		=> $this->UID
				,'user_type'	=> $this->session->userdata('USER')->user_type
				//,'ind_status'	=> 1 
		];
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			if($this->input->post('sSearch', TRUE) != '') {
				$search = $this->input->post('sSearch');
				$like = ['company_social' => $search];
				$result = $this->company->getCompany($where, $like, $limit, $start);
				$total = $this->company->getTotalCompany($where, $like, $limit, $start);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['company_social' => $search];
					$result = $this->company->getCompany($where, $like, $limit, $start);
					$total = $this->company->getTotalCompany($where, $like);
				} else {
					$result = $this->company->getCompany($where, null, $limit, $start);
					$total = $this->company->getTotalCompany($where);
				}
			}
		} else {
			$result = $this->company->getCompany($where);
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						if($r->company_status == 0x0001) {
							$show = createLink(base_url() . 'company/viewCompany/' . $r->company_id, 'btn-info', 'icon-eye-open', 'Ver');
							$edit = '&nbsp;' . createLink(base_url() . 'company/editCompany/' . $r->company_id, 'btn-success', 'icon-pencil', 'Editar');
							$del = '&nbsp;' . createLink(base_url() . 'company/deleteCompany/' . $r->company_id, 'btn-danger', 'icon-trash', 'Eliminar');
							$link = $show . $edit . $del;
							$rowClass = '';
						} else {
							$link = createLink(base_url() . 'company/recoveryCompany/' . $r->company_id, 'btn-warning', 'icon-ok', 'Recuperar');
							$rowClass = 'text-error';
						}
						
						array_push($records, [
							'DT_RowId'	=> $r->company_id
							,'DT_RowClass' => $rowClass
							,0	=> $r->company_id
							,1	=> $r->company_social
							,2	=> $r->company_ruc
							,3	=> $link
						]);
					}
					$data = ['sEcho' => $sEcho
						,'iTotalRecords' => $total
						,'iTotalDisplayRecords' => $total
						,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				
			break;
		}
	}
	
	public function searchCompany() {
		$field = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$permit = ['social', 'ruc'];
		$search = $_REQUEST['query'];
		$data = [];
		$data['query'] = $search;
		$suggestion = [];
		if(strlen($search) >= 0x0003) {
			if(in_array($field, $permit)) {
				$result = $this->company->searchCompany($search, $field);
				$items = [];
				foreach($result as $k) {
					$value = ($field == 'social') ? $k->des_social : $k->des_ruc;
					$items[] = [
							'value'				=> $value
							,'data'				=> $k->des_ruc
							,'company_id'		=> $k->company_id
							,'des_social'		=> $k->des_social
							,'des_comercial'	=> $k->des_comercial
							,'des_ruc'			=> $k->des_ruc
							,'des_direccion'	=> $k->des_direccion
					];
				}
				$suggestion = $items;
			}
		}
		$data['suggestions'] = $suggestion;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode( $data ));
	}
}
