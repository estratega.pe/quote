<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Local extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('local/localModel', 'local');
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
	}
	
	public function index()
	{
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'LOCAL']
				,['LABEL' => 'EMPRESA']
				,['LABEL' => 'ACCIONES']
		];
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Locales', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Locales'
			,'URL_AJAX'			=> base_url() . 'local/getLocal/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Locales'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'local_id'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> createLink(base_url() . 'local/addLocal', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'TH_TABLE'			=> $columnas
		], TRUE);
		renderPage($data);
	}
	
	public function addLocal() {
		$subtitle = 'Crear nuevo local';
		$bg_color = 'blue';
		$local_name = '';
		$local_address = '';
		$local_email = '';
		$local_phone = '';
		$company_id = '';
		$company_status = 1;
		if( $this->input->post() ) {
			$local_name = $this->input->post('input_local_name', TRUE);
			$company_id = $this->input->post('select_company_id', TRUE);
			$local_address = $this->input->post('textarea_local_address', TRUE);
			$local_email = $this->input->post('input_local_email', TRUE);
			$local_phone = $this->input->post('input_local_phone', TRUE);
			if( empty($local_name) || empty($local_address) || empty($company_id) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'local_name'		=> $local_name
						,'local_address'	=> $local_address
						,'local_email'		=> $local_email
						,'local_phone'		=> $local_phone
						,'company_id'		=> $company_id
				];
				$newID = $this->gen->saveData('jyc_local', $data);
				if(  is_numeric($newID) ) {
					redirect(base_url() . 'local/viewLocal/' . $newID);
				} else {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				}
			}
			
		}
		$select_company_id = [];
		$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
		foreach($c as $k) {
			$select_company_id[$k->company_id] = $k->company_social;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Local', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Nuevo Local';
		$data['BODY'] = $this->parser->parse('local/addLocal', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Local'
			,'BODY_SUBTITLE'			=> $subtitle
			,'BODY_MENU'				=> ''
			,'BG_COLOR'					=> $bg_color
			,'URL_POST'					=> base_url() .'local/addLocal/'
			,'INPUT_LOCAL_NAME'			=> form_input(array('name' => 'input_local_name', 'id' => 'input_local_name', 'maxlength' => '150'), $local_address, 'class="span6" required ')
			,'INPUT_LOCAL_EMAIL'		=> form_input(array('name' => 'input_local_email', 'type' => 'email', 'id' => 'input_local_email', ), $local_email, 'class="span6" ' )
			,'INPUT_LOCAL_PHONE'		=> form_input(array('name' => 'input_local_phone', 'type' => 'number', 'id' => 'input_local_phone', 'min' => '1000000', 'max' => '999999999'), $local_phone, 'class="span6" ' )
			,'TEXTAREA_LOCAL_ADDRESS'	=> form_textarea(array('name' => 'textarea_local_address', 'id' => 'textarea_local_address', 'maxlength' => '300'), nl2br($local_address), 'class="span6" required ')
			,'SELECT_COMPANY_ID'		=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-success', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'local', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editLocal() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		
		$where = [
				'local_id'		=> $ID
				,'user_id'		=> $this->UID
				,'user_type'	=> $this->UTYPE
		];
		$c = $this->local->getRowLocal($where);
		if(count($c) != 0x0001) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$subtitle = 'Editar local';
		$bg_color = 'blue';
		$local_name = $c->local_name;
		$local_address = $c->local_address;
		$local_email = $c->local_email;
		$local_phone = $c->local_phone;
		$company_id = $c->company_id;
		$local_status = 1;
		if( $this->input->post() ) {
			$local_name = $this->input->post('input_local_name', TRUE);
			$company_id = $this->input->post('select_company_id', TRUE);
			$local_address = $this->input->post('textarea_local_address', TRUE);
			$local_email = $this->input->post('input_local_email', TRUE);
			$local_phone = $this->input->post('input_local_phone', TRUE);
			if( empty($local_name) || empty($local_address) || empty($company_id) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'local_name'		=> $local_name
						,'local_address'	=> $local_address
						,'local_email'		=> $local_email
						,'local_phone'		=> $local_phone
						,'company_id'		=> $company_id
				];
				$where = [
						'local_id'	=> $ID
				];
				if(  !$this->gen->updateData('jyc_local', $data, $where) ) {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'local/viewLocal/' . $ID);
				}
			}
				
		}
		$select_company_id = [];
		$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
		foreach($c as $k) {
			$select_company_id[$k->company_id] = $k->company_social;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Local', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Nuevo Local';
		$data['BODY'] = $this->parser->parse('local/editLocal', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Editar Local'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'URL_POST'					=> base_url() .'local/editLocal/' . $ID
				,'INPUT_LOCAL_NAME'			=> form_input(array('name' => 'input_local_name', 'id' => 'input_local_name', 'maxlength' => '150'), $local_name, 'class="span6" required ')
				,'INPUT_LOCAL_EMAIL'		=> form_input(array('name' => 'input_local_email', 'type' => 'email', 'id' => 'input_local_email', ), $local_email, 'class="span6" ' )
				,'INPUT_LOCAL_PHONE'		=> form_input(array('name' => 'input_local_phone', 'type' => 'number', 'id' => 'input_local_phone', 'min' => '1000000', 'max' => '999999999'), $local_phone, 'class="span6" ' )
				,'TEXTAREA_LOCAL_ADDRESS'	=> form_textarea(array('name' => 'textarea_local_address', 'id' => 'textarea_local_address', 'maxlength' => '300'), nl2br($local_address), 'class="span6" required ')
				,'SELECT_COMPANY_ID'		=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-success', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'local', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewLocal() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$subtitle = 'Ver Local';
		$bg_color = 'blue';
		$where = [
				'local_id'		=> $ID
				,'user_id'		=> $this->UID
				,'user_type'	=> $this->UTYPE
		];
		$c = $this->local->getRowLocal($where);
		if(count($c) != 0x0001) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Local', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Ver Local';
		$data['BODY'] = $this->parser->parse('local/viewLocal', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Ver Local'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'URL_POST'					=> base_url() .'local/addLocal/'
				,'INPUT_LOCAL_NAME'			=> $c->local_name
				,'INPUT_LOCAL_EMAIL'		=> $c->local_email
				,'INPUT_LOCAL_PHONE'		=> $c->local_phone
				,'TEXTAREA_LOCAL_ADDRESS'	=> nl2br($c->local_address)
				,'SELECT_COMPANY_ID'		=> $c->company_social
				,'LINK_BACK'				=> createLink(base_url() . 'local', ' ', 'icon-ban', 'regresar al listado', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteLocal() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$where = [
				'local_id'		=> $ID
				,'user_id'		=> $this->UID
				,'user_type'	=> $this->UTYPE
		];
		$c = $this->local->getRowLocal($where);
		if(count($c) != 0x0001) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n';
		if($this->input->post()) {
			if( $this->input->post('input_local_id') == $ID) {
				$data = [
						'local_status'	=> 0
				];
				$where = [
						'local_id'	=> $this->input->post('input_local_id')
				];
				if (!$this->gen->updateData('jyc_local', $data, $where) ) {
					$this->db->trans_rollback();
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'local');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Local ' . $c->local_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Local - ' . $c->local_name
			,'BODY_SUBTITLE'			=> $subtitle
			,'BODY_MENU'				=> '(' . $c->local_name . ')'
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del local "' . $c->local_name . '"'
			,'BG_COLOR'					=> $bg_color
			,'URL_POST'					=> base_url() . 'local/deleteLocal/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_local_id', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'company', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function recoveryLocal() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'company');
			exit();
		}
		$where = [
				'local_id'		=> $ID
				,'user_id'		=> $this->UID
				,'user_type'	=> $this->UTYPE
		];
		$c = $this->local->getRowLocal($where);
		if(count($c) >= 0x0001 && ($c->local_id == $ID) ) {
			$data = [
					'local_status'	=> 1
			];
			$where = [
					'local_id'	=> $ID
			];
			if (!$this->gen->updateData('jyc_local', $data, $where) ) {
				$this->db->trans_rollback();
			} else {
					
			}
		}
	
		redirect(base_url() . 'local');
	}
	
	public function getLocal() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$where = [
				'rel_company_user.user_id'		=> $this->UID
				,'rel_company_user.user_type'	=> $this->UTYPE
				//,'jyccompany.ind_status'	=> 1 
		];
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			if($this->input->post('sSearch', TRUE) != '') {
				$search = $this->input->post('sSearch');
				$like = ['local_name' => $search];
				$result = $this->local->getLocal($where, $like, $limit, $start);
				$total = $this->local->getTotalLocal($where, $like, $limit, $start);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['local_name' => $search];
					$result = $this->local->getLocal($where, $like, $limit, $start);
					$total = $this->local->getTotalLocal($where, $like);
				} else {
					$result = $this->local->getLocal($where, null, $limit, $start);
					$total = $this->local->getTotalLocal($where);
				}
			}
		} else {
			$result = $this->local->getLocal($where);
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						if($r->company_status == 0x0001) {
							$show = createLink(base_url() . 'local/viewLocal/' . $r->local_id, 'btn-info', 'icon-eye-open', 'Ver');
							$edit = '&nbsp;' . createLink(base_url() . 'local/editLocal/' . $r->local_id, 'btn-success', 'icon-pencil', 'Editar');
							$del = '&nbsp;' . createLink(base_url() . 'local/deleteLocal/' . $r->local_id, 'btn-danger', 'icon-trash', 'Eliminar');
							$link = $show . $edit . $del;
							$rowClass = '';
							$empresa = $r->company_social;
						} else {
							$link = 'Recupere la empresa primero';
							$rowClass = 'text-error';
							$empresa = 'EMPRESA ELIMINADA';
						}
						if(!$r->local_status == 0x0001) {
							$rowClass = 'text-error';
							$link = createLink(base_url() . 'local/recoveryLocal/' . $r->local_id, 'btn-warning', 'icon-ok', 'Recuperar');
						}
						
						array_push($records, [
							'DT_RowId'	=> $r->local_id
							,'DT_RowClass' => $rowClass
							,0	=> $r->local_id
							,1	=> $r->local_name
							,2	=> $empresa
							,3	=> $link
						]);
					}
					$data = ['sEcho' => $sEcho
						,'iTotalRecords' => $total
						,'iTotalDisplayRecords' => $total
						,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $result ));
			break;
		}
	}
	
	public function getLocalOptions($where) {
		if($this->UTYPE == $this->config->item('IND_ADMIN')) {
			$result = $this->local->getLocal($where);
		} else {
			$this->db->from('jyc_local');
			$this->db->join('rel_user_local', 'rel_user_local.local_id=jyc_local.local_id');
			$this->db->where(['rel_user_local.company_id' => $where['rel_company_user.company_id']
					,'rel_user_local.user_id'	=> $where['rel_company_user.user_id']
					,'rel_user_local.user_type'	=> $where['rel_company_user.user_type']
			]);
			$result = $this->db->get()->result();
		}
		$html = "";
		foreach($result as $l) {
			$html .= "<option value='$l->local_id'>$l->local_name</option>";
		}
		$this->output
		->set_content_type('application/html')
		->set_output( $html );
	}
}
