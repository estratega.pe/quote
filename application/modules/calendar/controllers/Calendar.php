<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('calendar/calendarModel', 'calendar');
		$this->load->model('company/companyModel', 'company');
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
	}
	
	public function index()
	{
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'RAZON SOCIAL']
				,['LABEL' => 'RUC']
				,['LABEL' => 'ACCIONES']
		];
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Calendario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('calendar/homeCalendar', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Calendario'
		], TRUE);
		$data['FOOTER_EXTRA'] = '
				<script src="' . base_url() . 'assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
				<script src="' . base_url() . 'calendar/getScript"></script>';
		renderPage($data);
	}
	
	public function addQuote() {
		$subtitle = 'Crear nueva cita';
		$bg_color = 'blue';
		
		$u = $this->gen->getRow('gen_user', ['user_id' => $this->UID]);
		
		$company_id = $u->company_id;
		$local_id = $u->local_id;
		$disabled_local =  (empty($u->local_id)) ? 'disabled' : '';
		$space_id = '';
		$disabled_space =  (empty($local_id)) ? 'disabled' : '';
		$fec_start = '';
		$hour_start = '';
		$hour_end = '';
		$quote_description = '';
		$quote_title = '';
		
		if(empty($company_id) || empty($local_id) ) {
			$module = (empty($company_id)) ? 'Empresa' : 'Local';
			$module_link = (empty($company_id)) ? 'company/addCompany' : 'local/addLocal';
			if($u->user_type == $this->config->item('IND_ADMIN')) {
				$title = 'Cree ' . $module . ' primero';
				$subtitle = 'Cree ' . $module . ' primero';
				$bg_color = 'red';
				$link = createLink(base_url() . $module_link, ' ', 'icon-ban', 'Crear ' . $module, true);
			} else {
				$title = 'Solicite a su administrador que cree ' . $module . ' primero.';
				$subtitle = 'Solicite a su administrador que cree ' . $module . ' primero.';
				$bg_color = 'red';
				$link = '';
			}
				
			$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME Cree una empresa primero', 'BASE_URL' => base_url()];
			$data['BODY'] = $this->parser->parse('notice', ['BASE_URL' => base_url()
					,'BODY_TITLE'			=> $title
					,'BODY_SUBTITLE'		=> $subtitle
					,'BODY_MENU'			=> ''
					,'BG_COLOR'				=> $bg_color
					,'NOTICE'				=> $link
			], TRUE);
			renderPage($data);
				
		} else {
			if( $this->input->post() ) {
				$company_id = $this->input->post('select_company_id');
				$local_id = $this->input->post('select_local_id');
				$space_id = $this->input->post('select_space_id');
				$fec_start = $this->input->post('input_fec_start', TRUE);
				$hour_start = $this->input->post('input_hour_start', TRUE);
				$hour_end = $this->input->post('input_hour_end', TRUE);
				$quote_description = $this->input->post('textarea_quote_description', TRUE);
				$customer_id = $this->input->post('input_customer_id');
				$employee_id = $this->input->post('input_employee_id');
				$quote_title = $this->input->post('input_quote_title', TRUE);
					
				if(empty($company_id) || empty($local_id) || empty($fec_start) || empty($hour_end) || empty($hour_start) || empty($customer_id) || empty($quote_title) ) {
					$subtitle = 'Datos incompletos, los campos marcados con (*) son obligatorios.';
					$bg_color = 'red';
				} else {
					$quote_fec_start = converDate($fec_start);
					$quote_fec_end = converDate($fec_start);
					$quote_data = [
							'quote_fec_start'		=> $quote_fec_start . ' ' . $hour_start
							,'quote_fec_end'		=> $quote_fec_end . ' ' . $hour_end
							,'quote_title'			=> $quote_title
							,'quote_description'	=> $quote_description
							,'company_id'			=> $company_id
							,'local_id'				=> $local_id
					];
					if(!empty($space_id)) {
						$quote_data['space_id'] = $space_id;
					}
					$newID = $this->gen->saveData('jyc_quote', $quote_data);
					if(  is_numeric($newID) ) {
						$this->gen->saveData('rel_quote_participant', ['quote_id' => $newID
								,'user_id' 		=> $customer_id
								,'user_type'	=> $this->config->item('IND_CUSTOMER')
						]);
						if(!empty($employee_id)) {
							$this->gen->saveData('rel_quote_participant', ['quote_id' => $newID
									,'user_id' 		=> $employee_id
									,'user_type'	=> $this->config->item('IND_EMPLOYEE')
							]);
						}
						//redirect(base_url() . 'customer/viewCustomer/' . $newID);
						redirect(base_url() . 'calendar');
					} else {
						$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
						$bg_color = 'red';
					}
				}
			}
			
			$select_company_id = ['' => 'Seleccione'];
			$select_local_id = [];
			$select_space_id = ['' => 'Seleccione'];
			if($this->UTYPE == $this->config->item('IND_ADMIN')) {
				$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
				foreach($c as $k) {
					$select_company_id[$k->company_id] = $k->company_social;
				}
					
				if(!empty($company_id)) {
					$local = $this->gen->getData('jyc_local', ['company_id' => $company_id, 'local_status' => 1]);
					foreach($local as $l) {
						$select_local_id[$l->local_id] = $l->local_name;
					}
				}
			} else {
				$select_company_id[$u->company_id] = $this->gen->getRow('jyc_company', ['company_id' => $u->company_id])->company_social;
				if(!empty($company_id)) {
					$local = $this->gen->getData('rel_user_local', ['company_id' => $company_id, 'user_type' => $this->UTYPE, 'user_id' => $this->UID]);
					foreach($local as $l) {
						$select_local_id[$l->local_id] = $this->gen->getRow('jyc_local', ['local_id' => $l->local_id])->local_name;
					}
				}
			}
			if(!empty($local_id)) {
				$space = $this->gen->getData('jyc_space', ['local_id' => $local_id, 'company_id' => $company_id, 'space_status' => 1]);
				foreach($space as $s) {
					$select_space_id[$s->space_id] = $s->space_name;
				}
			}
			
			$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nueva Cita', 'BASE_URL' => base_url()];
			$data['BODY_TITLE'] = 'Nueva Cita';
			$data['BODY'] = $this->parser->parse('calendar/addCalendar', ['BASE_URL' => base_url()
					,'BODY_TITLE'				=> 'Nueva Cita'
					,'BODY_SUBTITLE'			=> $subtitle
					,'BODY_MENU'				=> ''
					,'BG_COLOR'					=> $bg_color
					,'DATE_TODAY'				=> date('d-m-Y')
					,'URL_POST'					=> base_url() .'calendar/addQuote/'
					,'SELECT_COMPANY_ID'		=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
					,'SELECT_LOCAL_ID'			=> form_dropdown(array('name' => 'select_local_id', 'id' => 'select_local_id'), $select_local_id, $local_id, 'class="span6" ' . $disabled_local . ' required')
					,'SELECT_SPACE_ID'			=> form_dropdown(array('name' => 'select_space_id', 'id' => 'select_space_id'), $select_space_id, $space_id, 'class="span6" ' . $disabled_space . ' ')
					,'INPUT_FEC_START'			=> form_input(array('name' => 'input_fec_start', 'id' => 'input_fec_start', 'size' => '16'), date('d-m-Y'), 'class="m-ctrl-small" readonly required')
					,'INPUT_HOUR_START'			=> form_input(array('name' => 'input_hour_start', 'id' => 'input_hour_start'), $hour_start, 'class="input-small" readonly required')
					,'INPUT_CUSTOMER_NAME'		=> form_input(array('name' => 'input_customer_name', 'id' => 'input_customer_name'), '', 'class="span6" required')
					,'INPUT_CUSTOMER_ID'		=> form_hidden('input_customer_id', '', 'id="input_customer_id"')
					,'INPUT_EMPLOYEE_NAME'		=> form_input(array('name' => 'input_employee_name', 'id' => 'input_employee_name'), '', 'class="span6" required')
					,'INPUT_EMPLOYEE_ID'		=> form_hidden('input_employee_id', '', 'id="input_employee_id"')
					,'INPUT_HOUR_END'			=> form_input(array('name' => 'input_hour_end', 'id' => 'input_hour_end'), $hour_end, 'class="input-small" readonly required')
					,'INPUT_QUOTE_TITLE'		=> form_input(array('name' => 'input_quote_title', 'id' => 'input_quote_title'), $quote_title, 'class="span6" required')
					,'TEXTAREA_DES_DESCRIPTION'	=> form_textarea(array('name' => 'textarea_quote_description', 'id' => 'textarea_quote_description', 'maxlength' => '300'), nl2br($quote_description), 'class="span6" ')
					,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-success', 'icon-save')
					,'BUTTON_CANCEL'			=> createLink(base_url() . 'calendar', ' ', 'icon-ban', 'Cancelar', true)
			], TRUE);
			$data['FOOTER_EXTRA'] = "
			
		<script>
		var Script = function () {
			if (top.location != location) {
				top.location.href = document.location.href ;
			}
			$(function(){
				$('#input_fec_start').datepicker({
					format: 'dd-mm-yyyy'
				});
				$('#input_hour_start, #input_hour_end').timepicker({
			        minuteStep: 1,
			        showSeconds: true,
			        showMeridian: false
			    });
			});
			$('#select_company_id').change(function() {
				ID = $(this).val();
				if(ID >= 1) {
					$.ajax({
					  url: '" . base_url() . "space/getLocalOptions/' + ID,
					  dataType: 'html'
					}).done(function(row){
						$('#select_local_id').html(row).attr('disabled', false);
					  	$('#select_local_id').html(row).attr('required', true);
					  	$('#select_space_id').html('').attr('disabled', true);
					});
				} else {
					$('#select_local_id').html('').attr('disabled', true);
					 $('#select_space_id').html('').attr('disabled', true);
				}
			
			});
			$('#select_local_id').change(function() {
				ID = $(this).val();
				COMPANY = $('#select_company_id').val();
				if(ID >= 1) {
					$.ajax({
					  url: '" . base_url() . "space/getSpaceOptions/' + ID + '/' + COMPANY,
					  dataType: 'html'
					}).done(function(row){
						$('#select_space_id').html(row).attr('disabled', false);
					  	$('#select_space_id').html(row).attr('required', true);
					});
				} else {
					$('#select_space_id').html('').attr('disabled', true);
				}
			
			});
			COMPANY = $('#select_company_id').val();
			$('#input_customer_name').autocomplete({
			    serviceUrl: '" . base_url() . "customer/searchCustomer/' + COMPANY + '/',
			    minChars: 3,
			    onSelect: function (suggestion) {
			        $('input[name=\"input_customer_id\"]').val(suggestion.user_id);
			    }
			});
			$('#input_employee_name').autocomplete({
			    serviceUrl: '" . base_url() . "employee/searchEmployee/' + COMPANY + '/',
			    minChars: 3,
			    onSelect: function (suggestion) {
			        $('input[name=\"input_employee_id\"]').val(suggestion.user_id);
			    }
			});
		}();
		</script>";
			renderPage($data);
		}	
	}
	
	public function viewEvent() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'calendar');
			exit();
		}
		$subtitle = 'Ver Cita';
		$bg_color = 'blue';
		$c = $this->calendar->getRowEvent($ID);
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'calendar');
			exit();
		}
		$quote_fec_start = $c->quote_fec_start;
		$quote_fec_end = $c->quote_fec_end;
		$quote_title = $c->quote_title;
		$quote_description = $c->quote_description;
		$company_id = $c->company_id;
		$local_id = $c->local_id;
		$space_id = $c->space_id;
		$company_social = $c->company_social;
		$local_name = $c->local_name;
		$space_name = (empty($space_id)) ? '' : $this->gen->getRow('jyc_space', ['space_id' => $space_id])->space_name;
		$fs = explode(' ', $quote_fec_start);
		$fe = explode(' ', $quote_fec_end);
		
		$customer_id = $this->gen->getRow('rel_quote_participant', ['quote_id' => $ID, 'user_type' => $this->config->item('IND_CUSTOMER')])->user_id;
		$_c = $this->gen->getRow('gen_user', ['user_id' => $customer_id, 'user_type' => $this->config->item('IND_CUSTOMER')]);
		$customer_name = $_c->user_firstname . ' ' . $_c->user_lastname;
		$employee_id = $this->gen->getRow('rel_quote_participant', ['quote_id' => $ID, 'user_type' => $this->config->item('IND_EMPLOYEE')])->user_id;
		$_e = $this->gen->getRow('gen_user', ['user_id' => $employee_id, 'user_type' => $this->config->item('IND_EMPLOYEE')]);
		$employee_name = $_e->user_firstname . ' ' . $_e->user_lastname;
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Cita', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Ver Cita';
		$data['BODY'] = $this->parser->parse('calendar/viewCalendar', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Ver Cita'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'DATE_TODAY'				=> converDate($fs[0])
				,'SELECT_COMPANY_ID'		=> $company_social
				,'SELECT_LOCAL_ID'			=> $local_name
				,'SELECT_SPACE_ID'			=> $space_name
				,'INPUT_FEC_START'			=> converDate($fs[0])
				,'INPUT_HOUR_START'			=> $fs[1]
				,'INPUT_HOUR_END'			=> $fe[1]
				,'INPUT_CUSTOMER_NAME'		=> createLink(base_url() . 'customer/viewCustomer/' . $customer_id, 'blue', '', $customer_name, true)
				,'INPUT_CUSTOMER_ID'		=> form_hidden('input_customer_id', $customer_id, 'id="input_customer_id"')
				,'INPUT_EMPLOYEE_NAME'		=> ($this->UTYPE == $this->config->item('IND_ADMIN')) ? createLink(base_url() . 'employee/viewEmployee/' . $employee_id, 'blue', '', $employee_name, true) : $employee_name
				,'INPUT_EMPLOYEE_ID'		=> form_hidden('input_employee_id', '', 'id="input_employee_id"')
				,'INPUT_QUOTE_TITLE'		=> $quote_title
				,'TEXTAREA_DES_DESCRIPTION'	=> nl2br($quote_description)
				,'INPUT_DELETE_EVENT'		=> createLink(base_url() . 'calendar/deleteEvent/' . $ID, 'blue', '', $quote_title, true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteEvent() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'customer');
			exit();
		}
		$c = $this->calendar->getRowEvent($ID);
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'calendar');
			exit();
		}
		
	}
	
	public function getScript() {
		$data = [];
		$data['BASE_URL'] = base_url();
		$u = $this->gen->getRow('gen_user', ['user_id' => $this->UID]);
		if( empty($u->company_id) || empty($u->local_id) ) {
			$company_id = 0;
			$local_id = 0;
		} else {
			$company_id = $u->company_id;
			$local_id = $u->local_id;
		}
		$data['COMPANY_ID'] = $company_id;
		$data['LOCAL_ID'] = $local_id;
		
		$this->parser->parse('calendar/script', $data);
	}
	
	public function getLocal() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$html = '';
		if( $ID == 0x0000 ) {
			$this->output
			->set_content_type('application/html')
			->set_output( $html );
		} else {
			Modules::run('local/getLocalOptions', $ID);
		}
	}
	
	public function getEvents() {
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$company_id = $this->input->post('company_id');
		$local_id = $this->input->post('local_id');
		$result = [];
		if(!empty($start) || !empty($end) || !empty($company_id) || !empty($local_id) ) {
			$_start = date('Y-m-d', $start);
			$_end = date('Y-m-d', $end);
			$where = ['quote_fec_start >=' => $_start . ' 00:00:00'
					,'quote_fec_end <=' => $_end . ' 23:59:00'
			];
			$events = $this->calendar->getEvents($company_id, $local_id, $where);
			foreach($events as $ev) {
				$data = [
						'id'		=> $ev->quote_id 
						,'start'	=>$ev->quote_fec_start
						,'end'		=> $ev->quote_fec_end
						,'title'	=> $ev->quote_title
						,'allDay'	=> false
						,'url'		=> base_url() . 'calendar/viewEvent/' . $ev->quote_id
				];
				array_push($result, $data);
			}
		}
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode( $result ));
	}
	
}
