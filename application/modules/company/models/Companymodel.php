<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class companyModel extends MX_Controller {
	
	const LOCAL = 'jyc_local';
	const EMPRESA = 'jyc_company';
	const R_P_C = 'rel_company_user';
	const DOC = 'gen_doc';
	const TYPE_USER = 'gen_type_user';
	const USER = 'gen_user';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getCompany($where = null, $like = null, $limit = null, $start = null) {
    	$this->db->select('*');
    	$this->db->from(self::EMPRESA);
    	$this->db->join(self::R_P_C, self::R_P_C . '.company_id = ' . self::EMPRESA . '.company_id');
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get()->result();
    }
	
	function getTotalCompany($where = null, $like = null, $start = null, $limit = null) {
		$this->db->from(self::EMPRESA);
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id = ' . self::EMPRESA . '.company_id');
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		return $this->db->count_all_results();
	}
	
	function getRowCompany($where = null) {
		$this->db->select('*');
		$this->db->from(self::R_P_C);
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id = ' . self::R_P_C . '.company_id');
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function searchCompany($search, $field = 'social') {
		$this->db->select(self::EMPRESA . '.*,'
				. self::R_P_C . '.company_id AS ' . self::R_P_C . '_company_id,'
				. self::R_P_C . '.user_id AS ' . self::R_P_C . '_user_id
		');
		$this->db->from(self::EMPRESA);
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id = ' . self::EMPRESA . '.company_id', 'left');
		$this->db->where(self::R_P_C . '.company_id');
		$this->db->where(self::R_P_C . '.user_id');
		$this->db->like('des_' . $field, $search);
		return $this->db->get()->result();
	}
	
	function saveCompany($data, $person) {
		$this->db->trans_start();
		$this->db->insert(self::EMPRESA, $data);
		$ID = $this->db->insert_id();
		$person['company_id'] = $ID;
		$this->db->insert(self::R_P_C, $person);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return $ID;
	}
}
