<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('employee/employeeModel', 'employee');
		$this->load->model('company/companyModel', 'company');
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
	}
	
	public function index()
	{
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'NOMBRES']
				,['LABEL' => 'APELLIDOS']
				,['LABEL' => 'EMPRESA']
				,['LABEL' => 'DOC']
				,['LABEL' => 'ACCIONES']
		];
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Trabajadores', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Trabajadores'
			,'URL_AJAX'			=> base_url() . 'employee/getEmployee/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Trabajadores'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'user_id'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> createLink(base_url() . 'employee/addEmployee', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'TH_TABLE'			=> $columnas
		], TRUE);
		renderPage($data);
	}
	
	public function addEmployee() {
		$subtitle = 'Ingresar nuevo Trabajador';
		$bg_color = 'blue';
		$user_firstname = '';
		$user_lastname = '';
		$user_email = '';
		$user_sex = '';
		$user_doc_type = '';
		$user_doc_number = '';
		$user_fec_start_t = '';
		$user_fec_nac = '';
		$company_id = '';
		$user_status = 1;
		if( $this->input->post() ) {
			$user_firstname = $this->input->post('input_user_firstname', TRUE);
			$user_lastname = $this->input->post('input_user_lastname', TRUE);
			$user_email = $this->input->post('input_user_email', TRUE);
			$user_sex= $this->input->post('select_user_sex');
			$user_doc_type = $this->input->post('select_user_doc_type');
			$user_doc_number = $this->input->post('input_user_doc_number');
			$user_fec_start_t = $this->input->post('input_user_fec_start_t', TRUE);
			$user_fec_nac = $this->input->post('input_user_fec_nac', TRUE);
			$company_id = $this->input->post('select_company_id');
			$local_id = $this->input->post('select_local_id');
			if(empty($user_firstname) || empty($user_email) || empty($company_id) || empty($user_sex) 
					|| empty($user_doc_number) || empty($user_doc_type) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$this->db->from('gen_user');
				$this->db->group_start();
				$this->db->where('user_email', $user_email);
				$this->db->or_where('user_login', $user_email);
				$this->db->group_end();
				$this->db->where('user_type', $this->config->item('IND_EMPLOYEE'));
				$uEmail = $this->db->count_all_results();
				if($uEmail >= 1) {
					$subtitle = 'Error El E-mail ya existe, por favor use otro.';
					$bg_color= 'red';
				} else {
					$_salt = Modules::run('login/createSalt');
					$_passwd = crypt($user_email, $_salt);
					$data = [
							'user_firstname'	=> $user_firstname
							,'user_lastname'	=> $user_lastname
							,'user_email'		=> $user_email
							,'user_sex'			=> $user_sex
							,'user_doc_type'	=> $user_doc_type
							,'user_doc_number'	=> $user_doc_number
							,'user_type'		=> $this->config->item('IND_EMPLOYEE')
							,'company_id'		=> $company_id
							,'user_aud_u_c'		=> $this->UID
							,'user_aud_fec_c'	=> date('Y-m-d H:i:s')
							,'user_login'		=> $user_email
							,'user_passwd'		=> $_passwd
							,'user_salt'		=> $_salt
					];
					if(!empty($user_fec_start_t))
						$data['user_fec_start_t'] = converDate($user_fec_start_t);
						if(!empty($user_fec_nac))
							$data['user_fec_nac'] = converDate($user_fec_nac);
					
							$dataRel = [
									'company_id'	=> $company_id
									,'user_type'	=> $this->config->item('IND_EMPLOYEE')
							];
							$newID = $this->employee->saveEmployee($data, $dataRel);
							if(  is_numeric($newID) ) {
								foreach($local_id as $l) {
									$this->gen->saveData('rel_user_local', ['user_id' => $newID
											,'user_type'	=> $this->config->item('IND_EMPLOYEE')
											,'company_id'	=> $company_id
											,'local_id'		=> $l
									]);
								}
								redirect(base_url() . 'employee/viewEmployee/' . $newID);
							} else {
								$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
								$bg_color = 'red';
							}
				}
			}
		}
		$select_company_id = ['' => 'Seleccione'];
		$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
		foreach($c as $k) {
			$select_company_id[$k->company_id] = $k->company_social;
		}
		$select_local_id = [];
		$select_doc_id = [];
		$d = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_DOC')]);
		foreach($d as $k) {
			$select_doc_id[$k->select_id] = $k->select_name;
		}
		$select_user_sex = ['' => 'Seleccione'];
		$s = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_SEX')]);
		foreach($s as $k) {
			$select_user_sex[$k->select_id] = $k->select_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Trabajador', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Nuevo Trabajador';
		$data['BODY'] = $this->parser->parse('employee/addEmployee', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Nuevo Trabajador'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'URL_POST'					=> base_url() .'employee/addEmployee/'
				,'INPUT_USER_FIRSTNAME'		=> form_input(array('name' => 'input_user_firstname', 'id' => 'input_user_firstname', 'maxlength' => '120'), $user_firstname, 'class="span6" required ')
				,'INPUT_USER_LASTNAME'		=> form_input(array('name' => 'input_user_lastname', 'id' => 'input_user_lastname', 'maxlength' => '120'), $user_lastname, 'class="span6" ')
				,'INPUT_USER_EMAIL'			=> form_input(array('name' => 'input_user_email', 'id' => 'input_user_email', 'maxlength' => '100'), $user_email, 'class="span6" required')
				,'SELECT_USER_SEX'			=> form_dropdown(array('name' => 'select_user_sex', 'id' => 'select_user_sex'), $select_user_sex, $user_sex, 'class="span6" required required')
				,'SELECT_USER_DOC_TYPE'		=> form_dropdown(array('name' => 'select_user_doc_type', 'id' => 'select_user_doc_type'), $select_doc_id, $user_doc_type, 'class="span6" required ')
				,'INPUT_USER_DOC_NUMBER'	=> form_input(array('name' => 'input_user_doc_number', 'id' => 'input_user_doc_number', 'maxlength' => '30'), $user_doc_number, 'class="span6" required ')
				,'INPUT_USER_FEC_START_T'	=> form_input(array('name' => 'input_user_fec_start_t', 'id' => 'input_user_fec_start_t', 'size' => '16'), $user_fec_start_t, 'class="m-ctrl-medium" readonly ')
				,'INPUT_USER_FEC_NAC'		=> form_input(array('name' => 'input_user_fec_nac', 'id' => 'input_user_fec_nac', 'size' => '16'), $user_fec_nac, 'class="m-ctrl-medium" readonly ')
				//,'TEXTAREA_DES_DIRECCION'	=> form_textarea(array('name' => 'textarea_des_direccion', 'id' => 'textarea_des_direccion', 'maxlength' => '300'), nl2br($des_direccion), 'class="span6" required ')
				,'SELECT_COMPANY_ID'		=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
				,'SELECT_LOCAL_ID'			=> form_dropdown(array('name' => 'select_local_id[]', 'id' => 'select_local_id[]'), $select_local_id, ['X','Y'], 'class="select_local_id span6" multiple disabled required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-success', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'employee', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$data['FOOTER_EXTRA'] = "<script>var Script = function () { 
			if (top.location != location) {
		        top.location.href = document.location.href ;
		    }
			$(function(){
				$('#dpYears').datepicker();
				$('#dpNac').datepicker();
			});
				
			$('#select_company_id').change(function() {
				ID = $(this).val();
				if(ID >= 1) {
					$.ajax({
						url: '" . base_url() . "space/getLocalOptions/' + ID,
						dataType: 'html'
					}).done(function(row){
						$('.select_local_id').html(row).attr('disabled', false);
					});
				} else {
					$('.select_local_id').html('').attr('disabled', true);
				}
			});
	}();</script>";
		renderPage($data);
	}
	
	public function editEmployee() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'employee');
			exit();
		}
		
		$c = $this->employee->getRowEmployee(['rel_company_employee.user_id' => $ID, 'gen_user.user_status' => 1]);
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'employee');
			exit();
		}
		$subtitle = 'Editar Trabajador';
		$bg_color = 'blue';
		$user_firstname = $c->user_firstname;
		$user_lastname = $c->user_lastname;
		$user_email = $c->user_email;
		$user_status = $c->user_status;
		$user_type = $c->user_type;
		$company_id = $c->company_id;
		$local_id = $c->local_id;
		$user_fec_start_t = !empty($c->user_fec_start_t) ? converDate($c->user_fec_start_t) : '';
		$user_fec_end_t = !empty($c->user_fec_end_t) ? converDate($c->user_fec_end_t) : '';
		$user_fec_nac = !empty($c->user_fec_nac) ? converDate($c->user_fec_nac) : '';
		$user_doc_type = $c->user_doc_type;
		$user_doc_number = $c->user_doc_number;
		$user_sex = $c->user_sex;
		if( $this->input->post() ) {
			$user_firstname = $this->input->post('input_user_firstname', TRUE);
			$user_lastname = $this->input->post('input_user_lastname', TRUE);
			$user_email = $this->input->post('input_user_email', TRUE);
			$user_sex= $this->input->post('select_user_sex');
			$user_doc_type = $this->input->post('select_user_doc_type');
			$user_doc_number = $this->input->post('input_user_doc_number');
			$user_fec_start_t = $this->input->post('input_user_fec_start_t', TRUE);
			$user_fec_end_t = $this->input->post('input_user_fec_end_t', TRUE);
			$user_fec_nac = $this->input->post('input_user_fec_nac', TRUE);
			$company_id = $this->input->post('select_company_id');
			$local_id = $this->input->post('select_local_id');
			$user_passwd = $this->input->post('input_user_passwd', TRUE);
			if(!empty($user_passwd)) {
				$_salt = Modules::run('login/createSalt');
				$_passwd = crypt($user_passwd, $_salt);
				$this->gen->updateData('gen_user', ['user_passwd' => $_passwd, 'user_salt' => $_salt],['user_id' => $ID]);
			}
			if(empty($user_firstname) || empty($user_email) || empty($company_id) || empty($user_sex)
					|| empty($user_doc_number) || empty($user_doc_type) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'user_firstname'	=> $user_firstname
						,'user_lastname'	=> $user_lastname
						,'user_email'		=> $user_email
						,'user_sex'			=> $user_sex
						,'user_doc_type'	=> $user_doc_type
						,'user_doc_number'	=> $user_doc_number
						,'company_id'		=> $company_id
				];
				if( !empty($user_fec_start_t) ) {
					$data['user_fec_start_t'] = converDate($user_fec_start_t);
				}
				if( !empty($user_fec_end_t) ) {
					$data['user_fec_end_t'] = converDate($user_fec_end_t);
				}
				if( !empty($user_fec_nac) ) {
					$data['user_fec_nac'] = converDate($user_fec_nac);
				}
				$where = [
						'user_id'	=> $ID
				];
				if(  !$this->gen->updateData('gen_user', $data, $where) ) {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					$this->gen->deleteData('rel_user_local', ['user_id' => $ID
							,'user_type'	=> $this->config->item('IND_EMPLOYEE')
							,'company_id'	=> $company_id
					]);
					foreach($local_id as $l) {
						$this->gen->saveData('rel_user_local', ['user_id' => $ID
								,'user_type'	=> $this->config->item('IND_EMPLOYEE')
								,'company_id'	=> $company_id
								,'local_id'		=> $l
						]);
					}
					redirect(base_url() . 'employee/viewEmployee/' . $ID);
				}
			}	
		}
		$select_company_id = ['' => 'Seleccione'];
		$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
		foreach($c as $k) {
			$select_company_id[$k->company_id] = $k->company_social;
		}
		$select_local_id = [];
		$sl = $this->gen->getData('jyc_local', ['company_id' => $company_id]);
		foreach($sl as $l) {
			$select_local_id[$l->local_id] = $l->local_name;
		}
		$selected_local = [];
		$sel = $this->gen->getData('rel_user_local', ['user_id' => $ID
				,'company_id'	=> $company_id
				,'user_type'	=> $this->config->item('IND_EMPLOYEE')
		]);
		foreach ($sel as $r) {
			array_push($selected_local, $r->local_id);
		}
		$select_doc_id = [];
		$d = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_DOC')]);
		foreach($d as $k) {
			$select_doc_id[$k->select_id] = $k->select_name;
		}
		$select_user_sex = ['' => 'Seleccione'];
		$s = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_SEX')]);
		foreach($s as $k) {
			$select_user_sex[$k->select_id] = $k->select_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Trabajador', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Editar Trabajador';
		$data['BODY'] = $this->parser->parse('employee/editEmployee', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Editar Trabajador'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'URL_POST'					=> base_url() .'employee/editEmployee/' . $ID
				,'INPUT_USER_FIRSTNAME'		=> form_input(array('name' => 'input_user_firstname', 'id' => 'input_user_firstname', 'maxlength' => '120'), $user_firstname, 'class="span6" required ')
				,'INPUT_USER_LASTNAME'		=> form_input(array('name' => 'input_user_lastname', 'id' => 'input_user_lastname', 'maxlength' => '120'), $user_lastname, 'class="span6" ')
				,'INPUT_USER_EMAIL'			=> form_input(array('name' => 'input_user_email', 'id' => 'input_user_email', 'maxlength' => '100'), $user_email, 'class="span6" required')
				,'SELECT_USER_SEX'			=> form_dropdown(array('name' => 'select_user_sex', 'id' => 'select_user_sex'), $select_user_sex, $user_sex, 'class="span6" required required')
				,'SELECT_USER_DOC_TYPE'		=> form_dropdown(array('name' => 'select_user_doc_type', 'id' => 'select_user_doc_type'), $select_doc_id, $user_doc_type, 'class="span6" required ')
				,'INPUT_USER_DOC_NUMBER'	=> form_input(array('name' => 'input_user_doc_number', 'id' => 'input_user_doc_number', 'maxlength' => '30'), $user_doc_number, 'class="span6" required ')
				,'INPUT_USER_FEC_START_T'	=> form_input(array('name' => 'input_user_fec_start_t', 'id' => 'input_user_fec_start_t', 'size' => '16'), $user_fec_start_t, 'class="m-ctrl-medium" readonly ')
				,'INPUT_USER_FEC_END_T'		=> form_input(array('name' => 'input_user_fec_end_t', 'id' => 'input_user_fec_end_t', 'size' => '16'), $user_fec_end_t, 'class="m-ctrl-medium" readonly ')
				,'INPUT_USER_FEC_NAC'		=> form_input(array('name' => 'input_user_fec_nac', 'id' => 'input_user_fec_nac', 'size' => '16'), $user_fec_nac, 'class="m-ctrl-medium" readonly ')
				//,'TEXTAREA_DES_DIRECCION'	=> form_textarea(array('name' => 'textarea_des_direccion', 'id' => 'textarea_des_direccion', 'maxlength' => '300'), nl2br($des_direccion), 'class="span6" required ')
				,'SELECT_COMPANY_ID'		=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
				,'SELECT_LOCAL_ID'			=> form_dropdown(array('name' => 'select_local_id[]', 'id' => 'select_local_id[]'), $select_local_id, $selected_local, 'class="select_local_id span6" multiple required')
				,'INPUT_USER_PASSWD'		=> form_input(array('name' => 'input_user_passwd', 'id' => 'input_user_passwd', 'maxlength' => '30'), '', 'class="span6" ')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-success', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'employee', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$data['FOOTER_EXTRA'] = "<script>var Script = function () {
			if (top.location != location) {
		        top.location.href = document.location.href ;
		    }
			$(function(){
				$('#dpYears').datepicker();
				$('#dpYearsE').datepicker();
				$('#dpNac').datepicker();
			});
			$('#select_company_id').change(function() {
				ID = $(this).val();
				if(ID >= 1) {
					$.ajax({
						url: '" . base_url() . "space/getLocalOptions/' + ID,
						dataType: 'html'
					}).done(function(row){
						$('.select_local_id').html(row).attr('disabled', false);
					});
				} else {
					$('.select_local_id').html('').attr('disabled', true);
				}
			});
	}();</script>";
		renderPage($data);
	}
	
	public function viewEmployee() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$subtitle = 'Ver Trabajor';
		$bg_color = 'blue';
		$c = $this->employee->getRowEmployee(['rel_company_employee.user_id' => $ID, 'gen_user.user_status' => 1]);
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'employee');
			exit();
		}
		$selected_local = "";
		$sel = $this->gen->getData('rel_user_local', ['user_id' => $ID
				,'company_id'	=> $c->company_id
				,'user_type'	=> $this->config->item('IND_EMPLOYEE')
		]);
		foreach ($sel as $l) {
			$selected_local .= '<span class="label">' . $this->gen->getRow('jyc_local', ['local_id' => $l->local_id])->local_name . '</span> &nbsp;';
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Trabajador', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Ver Trabajador';
		$data['BODY'] = $this->parser->parse('employee/viewEmployee', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Ver Trabajador'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'INPUT_USER_FIRSTNAME'		=> $c->user_firstname
				,'INPUT_USER_LASTNAME'		=> $c->user_lastname
				,'INPUT_USER_EMAIL'			=> $c->user_email
				,'SELECT_USER_SEX'			=> getRowSelect($c->user_sex)->select_name
				,'SELECT_USER_DOC_TYPE'		=> getRowSelect($c->user_doc_type)->select_name
				,'INPUT_USER_DOC_NUMBER'	=> $c->user_doc_number
				,'INPUT_USER_FEC_START_T'	=> (!empty($c->user_fec_start_t)) ? converDate($c->user_fec_start_t) : ''
				,'INPUT_USER_FEC_NAC'		=> (!empty($c->user_fec_nac)) ? converDate($c->user_fec_nac) : ''
				,'SELECT_COMPANY_ID'		=> $c->company_social
				,'SELECT_LOCAL_ID'			=> $selected_local
				,'LINK_BACK'				=> createLink(base_url() . 'employee', ' ', 'icon-ban', 'regresar al listado', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteEmployee() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'employee');
			exit();
		}
		$c = $this->employee->getRowEmployee(['rel_company_employee.user_id' => $ID, 'gen_user.user_status' => 1]);
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'employee');
			exit();
		}
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n';
		if($this->input->post()) {
			if( $this->input->post('input_employee_id') == $ID) {
				$data = [
						'user_status'	=> 0
				];
				$where = [
						'user_id'	=> $this->input->post('input_employee_id')
				];
				if (!$this->gen->updateData('gen_user', $data, $where) ) {
					$this->db->trans_rollback();
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'employee');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Trabajador ' . $c->user_firstname . ' ' . $c->user_lastname, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Local - ' . $c->user_firstname . ' ' . $c->user_lastname
			,'BODY_SUBTITLE'			=> $subtitle
			,'BODY_MENU'				=> '(' . $c->user_firstname . ' ' . $c->user_lastname . ')'
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Trabajador "' . $c->user_firstname . ' ' . $c->user_lastname . '"'
			,'BG_COLOR'					=> $bg_color
			,'URL_POST'					=> base_url() . 'employee/deleteEmployee/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_employee_id', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'employee', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function recoveryEmployee() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'employee');
			exit();
		}
		$c = $this->employee->getRowEmployee(['rel_company_employee.user_id' => $ID, 'gen_user.user_status' => 0]);
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'employee');
			exit();
		}
		if(count($c) >= 0x0001 && ($c->user_id == $ID) ) {
			$data = [
					'user_status'	=> 1
			];
			$where = [
					'user_id'	=> $ID
			];
			if (!$this->gen->updateData('gen_user', $data, $where) ) {
				$this->db->trans_rollback();
			}
		}
	
		redirect(base_url() . 'employee');
	}
	
	public function getEmployee() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$where = [
				'rel_company_user.user_id'		=> $this->UID
		];
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			if($this->input->post('sSearch', TRUE) != '') {
				$search = $this->input->post('sSearch');
				$like = ['user_firstname' => $search];
				$or_like = ['user_lastname' => $search, 'user_email' => $search, 'user_doc_number' => $search];
				$result = $this->employee->getEmployee($where, $like, $limit, $start, $or_like);
				$total = $this->employee->getTotalEmployee($where, $like, null, null, $or_like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['gen_user.user_firstname' => $search];
					$or_like = ['user_lastname' => $search, 'user_email' => $search, 'user_doc_number' => $search];
					$result = $this->employee->getEmployee($where, $like, $limit, $start, $or_like);
					$total = $this->employee->getTotalEmployee($where, $like, null, null, $or_like);
				} else {
					$result = $this->employee->getEmployee($where, null, $limit, $start);
					$total = $this->employee->getTotalEmployee($where);
				}
			}
		} else {
			$result = $this->employee->getEmployee($where);
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						if($r->company_status == 0x0001) {
							$show = createLink(base_url() . 'employee/viewEmployee/' . $r->user_id, 'btn-info', 'icon-eye-open', 'Ver');
							$edit = '&nbsp;' . createLink(base_url() . 'employee/editEmployee/' . $r->user_id, 'btn-success', 'icon-pencil', 'Editar');
							$del = '&nbsp;' . createLink(base_url() . 'employee/deleteEmployee/' . $r->user_id, 'btn-danger', 'icon-trash', 'Eliminar');
							$link = $show . $edit . $del;
							$rowClass = '';
							$empresa = $r->company_social;
						} else {
							$link = 'Recupere la empresa primero';
							$rowClass = 'text-error';
							$empresa = 'EMPRESA ELIMINADA';
						}
						if(!$r->user_status == 0x0001) {
							$rowClass = 'text-error';
							$link = createLink(base_url() . 'employee/recoveryEmployee/' . $r->user_id, 'btn-warning', 'icon-ok', 'Recuperar');
						}
						
						array_push($records, [
							'DT_RowId'	=> $r->user_id
							,'DT_RowClass' => $rowClass
							,0	=> $r->user_id
							,1	=> $r->user_firstname
							,2	=> $r->user_lastname
							,3	=> $empresa
							,4	=> $r->user_doc_number
							,5	=> $link
						]);
					}
					$data = ['sEcho' => $sEcho
						,'iTotalRecords' => $total
						,'iTotalDisplayRecords' => $total
						,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $result ));
			break;
		}
	}
	
	public function searchEmployee() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		//$search = $_REQUEST['query'];
		$search = $this->input->get('query', TRUE);
		$data = [];
		$data['query'] = $search;
		$suggestion = [];
		if(strlen($search) >= 0x0003) {
			$result = $this->employee->searchEmployee($search, $ID);
			$items = [];
			foreach($result as $k) {
				$value = $k->user_firstname . ' ' . $k->user_lastname;
				$items[] = [
						'value'				=> $value
						,'data'				=> $k->user_id
						,'user_id'			=> $k->user_id
						,'user_firstname'	=> $k->user_firstname
						,'user_lastname'	=> $k->user_lastname
				];
			}
			$suggestion = $items;
		}
		$data['suggestions'] = $suggestion;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode( $data ));
	}
}
