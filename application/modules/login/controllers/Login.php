<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('login/loginModel', 'login');
	}
	
	public function index()
	{
		if($this->session->userdata('USER'))
			redirect(base_url());
		$body['PAGE_LOGIN_TITLE'] = 'Ingresar al sistema';
		$body['URL_POST'] = base_url() .'login/loginIn';
		$body['BASE_URL'] = base_url();
		$this->parser->parse('login/login', $body);
	}
	
	public function loginIn()
	{
		if($this->input->post())
		{
			$usuario	= $this->security->xss_clean($this->input->post('usuario', TRUE));
			$password	= $this->security->xss_clean($this->input->post('contrasena', TRUE));
			if(trim($usuario) == '' || trim($password) == '') {
				redirect(base_url('login'));
				exit();
			} else {
				$usr = $this->_preLogin($usuario);
				if(sizeof($usr) == 0x0001) {
					if( crypt($password, $usr->user_salt) == $usr->user_passwd ) {
						$this->session->set_userdata('USER', $usr);
						redirect(base_url() . 'login/firstSetup');
						exit();
					} else {
						redirect(base_url('login'));
						exit();
					}
				} else {
					redirect(base_url('login'));
					exit();
				}
			}
		} else {
			redirect(base_url('login'));
		}
	}
	
	public function loginOut()
	{
		$this->session->unset_userdata('USER');
		$this->session->unset_userdata('PERMS');
		redirect(base_url('login'));
	}
	
	public function register() {
		$subtitle = 'Registro';
		$bg_color= 'blue';
		$terminos = '';
		#VARS
		$user_firstname = '';
		$user_lastname = '';
		$user_email = '';
		$user_sex = '';
		$user_doc_type = '';
		$user_doc_number = '';
		if($_POST) {
			$user_firstname	= $this->input->post('input_user_firstname', TRUE);
			$user_lastname	= $this->input->post('input_user_lastname', TRUE);
			$user_email		= $this->input->post('input_user_email', TRUE);
			$user_sex		= $this->input->post('select_user_sex');
			$user_doc_type	= $this->input->post('select_user_doc_type');
			$user_doc_number= $this->input->post('input_user_doc_number');
			$user_passwd	= $this->input->post('input_user_passwd', TRUE);
			$term_cond		= $this->input->post('term_cond');
			if(empty($user_firstname) || empty($user_lastname) || empty($user_email) || empty($user_doc_type) || 
					empty($user_doc_number) || empty($user_sex) || empty($user_passwd) ) {
				$subtitle = 'Registro :: Error Todos los campos son obligatorios.';
				$bg_color= 'red';
			} else {
				if(!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
					$subtitle = 'Registro :: Error El E-mail no es v&aacute;lido.';
					$bg_color= 'red';
				} else {
					$this->db->from('gen_user');
					$this->db->where('user_login', $user_email);
					$this->db->group_start();
					$this->db->where('user_type', $this->config->item('IND_ADMIN'));
					$this->db->or_where('user_type', $this->config->item('IND_EMPLOYEE'));
					$this->db->group_end();
					$uEmail = $this->db->count_all_results();
					if($uEmail >= 1) {
						$subtitle = 'Registro :: Error El E-mail ya existe, por favor use otro.';
						$bg_color= 'red';
					} else {
						$_salt = $this->createSalt();
						$_passwd = crypt($user_passwd, $_salt);
							
						$data = [
								'user_firstname'	=> $user_firstname
								,'user_lastname'	=> $user_lastname
								,'user_email'		=> $user_email
								,'user_login'		=> $user_email
								,'user_passwd'		=> $_passwd
								,'user_salt'		=> $_salt
								,'user_status'		=> 1
								,'user_type'		=> $this->config->item('IND_ADMIN')
								,'user_doc_type'	=> $user_doc_type
								,'user_doc_number'	=> $user_doc_number
								,'user_sex'			=> $user_sex
								,'user_aud_fec_c'	=> date('Y-m-d H:i:s')
						];
						$newID = $this->gen->saveData('gen_user', $data);
						if(is_numeric($newID)) {
							$usr = $this->db->get_where('gen_user', ['user_id' => $newID])->row();
							$this->session->set_userdata('USER', $usr);
							redirect(base_url());
							exit();
						} else {
							$subtitle = 'Registro :: Error En este momento no podemos proccesar su solicitud, por favor int&eacute;ntelo m&aacute;s tade..';
							$bg_color= 'red';
						}
					}
				}
			}
		}
		# TIPO DOC
		$select_doc_id = [];
		$d = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_DOC')]);
		foreach($d as $k) {
			$select_doc_id[$k->select_id] = $k->select_name;
		}
		# SEXO
		$select_user_sex = ['' => 'Seleccione'];
		$s = $this->gen->getData('gen_select', ['category_id' => $this->config->item('SELECT_TYPE_SEX')]);
		foreach($s as $k) {
			$select_user_sex[$k->select_id] = $k->select_name;
		}
		
		$data['BODY'] = $this->parser->parse('login/register', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Registro'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'URL_POST'					=> base_url() .'login/register/'
				,'INPUT_USER_FIRSTNAME'		=> form_input(array('name' => 'input_user_firstname', 'id' => 'input_user_firstname', 'maxlength' => '120'), $user_firstname, 'class="span6" required ')
				,'INPUT_USER_LASTNAME'		=> form_input(array('name' => 'input_user_lastname', 'id' => 'input_user_lastname', 'maxlength' => '120'), $user_lastname, 'class="span6" required')
				,'INPUT_USER_EMAIL'			=> form_input(array('type' => 'email', 'name' => 'input_user_email', 'id' => 'input_user_email', 'maxlength' => '100'), $user_email, 'class="span6" required')
				,'SELECT_USER_DOC_TYPE'		=> form_dropdown(array('name' => 'select_user_doc_type', 'id' => 'select_user_doc_type'), $select_doc_id, $user_doc_type, 'class="span6" required ')
				,'INPUT_USER_DOC_NUMBER'	=> form_input(array('name' => 'input_user_doc_number', 'id' => 'input_user_doc_number', 'maxlength' => '30'), $user_doc_number, 'class="span6" required ')
				,'SELECT_USER_SEX'			=> form_dropdown(array('name' => 'select_user_sex', 'id' => 'select_user_sex'), $select_user_sex, $user_sex, 'class="span6" required required')
				,'INPUT_USER_PASSWD'		=> form_input(array('name' => 'input_user_passwd', 'id' => 'input_user_passwd', 'maxlength' => '30'), '', 'class="span6" required ')
				//,'TEXTAREA_TERM_COND'		=> form_textarea('textarea_term_cond', nl2br($terminos), 'class="span6" readonly ')
				//,'CHECK_TERM_COND'			=> form_checkbox('term_cond', '1')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Registrarme', 'btn-success', 'icon-register')
				,'BUTTON_CANCEL'			=> createLink(base_url(), ' ', 'icon-ban', 'Cancelar', true)
		]);
	}
	
	
	public function createPassword($password)
	{
		$salt = $this->createSalt();
		$newPassword = crypt($password, $salt);
		return $newPassword;
	}
	
	private function _preLogin($user_email) {
		$this->db->from('gen_user');
		$this->db->where('user_login', $user_email);
		$this->db->where('user_status', 1);
		$this->db->group_start();
		$this->db->where('user_type', $this->config->item('IND_ADMIN'));
		$this->db->or_where('user_type', $this->config->item('IND_EMPLOYEE'));
		$this->db->group_end();
		return $this->db->get()->row();
	}
	
	public function createSalt()
	{
		$_crypt = '$6$rounds=' . mt_rand(1000, 10000) . '$';
		$_aString = './_-1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$salt = $_crypt;
		for($i = 0; $i < 25; $i++)
		{
			$salt .= $_aString[mt_rand(0, 65)];
		}
		$salt .= '$';
		return $salt;
	}
	
	public function firstSetup() {
		if($this->session->userdata('USER') && ( $this->session->userdata('USER')->user_type == $this->config->item('IND_ADMIN') ) ) {
			$this->db->from('jyc_company');
			$this->db->join('rel_company_user', 'rel_company_user.company_id=jyc_company.company_id');
			$this->db->where('rel_company_user.user_id', $this->session->userdata('USER')->user_id);
			$this->db->where('jyc_company.company_status', 1);
			$totalC = $this->db->count_all_results();
			
			if($totalC <= 0x0000) {
				$title = 'Configurando cuenta.';
				$subtitle = 'Felicidades, se ha registrado correctamente, vamos a configurar su cuenta.';
				$bg_color= 'blue';
					
				if($_POST) {
					$company_social		= $this->input->post('company_social', TRUE);
					$company_comercial	= $this->input->post('company_comercial', TRUE);
					$company_ruc		= $this->input->post('company_ruc', TRUE);
					$company_email		= $this->input->post('company_email', TRUE);
					$company_phone		= $this->input->post('company_phone', TRUE);
					$company_address	= $this->input->post('company_address', TRUE);
					
					if(empty($company_social) || empty($company_ruc) || empty($company_email) || empty($company_address) ) {
						$title = 'Error Configurando cuenta.';
						$subtitle = 'Favor de rellenar los campos obligatorios.';
						$bg_color= 'red';
					} else {
						if(!filter_var($company_email, FILTER_VALIDATE_EMAIL)) {
							$title = 'Error Configurando cuenta.';
							$subtitle = 'ERROR :: El E-mail no es v&aacute;lido.';
							$bg_color= 'red';
						} else {
							$this->db->trans_start();
							$this->db->insert('jyc_company', [
									'company_social'		=> $company_social
									,'company_comercial'	=> $company_comercial
									,'company_ruc'			=> $company_ruc
									,'company_address'		=> $company_address
									,'company_phone'		=> $company_phone
									,'company_email'		=> $company_email
									,'company_status'		=> 1
							]);
							$companyID = $this->db->insert_id();
							$this->db->insert('rel_company_user', [
									'company_id'	=> $companyID
									,'user_id'		=> $this->session->userdata('USER')->user_id
									,'user_type'	=> $this->config->item('IND_ADMIN')
							]);
							$this->db->insert('jyc_local', [
									'local_name'		=> $company_social
									,'local_address'	=> $company_address
									,'local_phone'		=> $company_phone
									,'local_email'		=> $company_email
									,'local_status'		=> 1
									,'company_id'		=> $companyID
							]);
							$localID = $this->db->insert_id();
							$this->db->insert('jyc_space', [
									'space_name'	=> 'POR ASIGNAR'
									,'space_status'	=> 1
									,'local_id'		=> $localID
									,'company_id'	=> $companyID
							]);
							$this->db->update('gen_user', [
									'company_id'	=> $companyID
									,'local_id'		=> $localID
							], ['user_id' => $this->session->userdata('USER')->user_id]);
							$this->db->trans_complete();
							if ($this->db->trans_status() === FALSE) {
								$title = 'ERROR Configurando cuenta.';
								$subtitle = 'ERROR  - No se ha podido procesar su solicitud, por favor intentelo m&aacute;s tarde.';
								$bg_color= 'red';
							} else {
								redirect(base_url());
							}
						}
					}
				}
					
				$data['BODY'] = $this->parser->parse('login/firstSetup', ['BASE_URL' => base_url()
						,'BODY_TITLE'				=> $title
						,'BODY_SUBTITLE'			=> $subtitle
						,'BODY_MENU'				=> ''
						,'BG_COLOR'					=> $bg_color
						,'URL_POST'					=> base_url() .'login/firstSetup/'
						,'BUTTON_SUBMIT'			=> createSubmitButton('Registrarme', 'btn-success', 'icon-register')
						,'BUTTON_CANCEL'			=> createLink(base_url(), ' ', 'icon-ban', 'Cancelar', true)
				]);
			} else {
				redirect(base_url());
			}
		} else {
			redirect(base_url());
		}
	}
}
