<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portal {
	
	private $CI;
	
	public function __construct() {
		$this->CI =& get_instance();
	}
	
	public function checkLogin() {
		if($this->CI->uri->segment(1) != 'login' && !$this->CI->session->userdata('USER')) {
			header('Location: ' . site_url() . 'login');
			exit();
		}
	}
	public function checkSetup() {
		if($this->CI->uri->segment(1) != 'login' && $this->CI->session->userdata('USER')) {
			if($this->CI->session->userdata('USER')->user_type == $this->CI->config->item('IND_ADMIN')) {
				$this->CI->db->from('jyc_company');
				$this->CI->db->join('rel_company_user', 'rel_company_user.company_id=jyc_company.company_id');
				$this->CI->db->where('rel_company_user.user_id', $this->CI->session->userdata('USER')->user_id);
				$this->CI->db->where('jyc_company.company_status', 1);
				$totalC = $this->CI->db->count_all_results();
				if($totalC <= 0x0000) {
					redirect(base_url() . 'login/firstSetup');
					exit();
				}	
			}
		}
	}
}

// END Portal class

/* End of file Portal.php */
/* Location: ./applicaton/hooks/Portal.php */