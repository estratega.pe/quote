<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class spaceModel extends MX_Controller {
	
	const SPACE = 'jyc_space';
	const LOCAL = 'jyc_local';
	const EMPRESA = 'jyc_company';
	const R_P_C = 'rel_company_user';
	
    function __construct()
    {
        parent::__construct();
        $this->load->model('company/companyModel', 'company');
    }
	
	function getSpace($where = null, $like = null, $limit = null, $start = null) {
		$this->db->select('*');
		$this->db->from(self::SPACE);
		$this->db->join(self::LOCAL, self::LOCAL . '.local_id=' . self::SPACE . '.local_id');
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id=' . self::SPACE . '.company_id');
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id=' . self::SPACE . '.company_id');
		$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get()->result();
	}
	
	function getTotalSpace($where = null, $like = null, $limit = null, $start = null) {
		$this->db->select('*');
		$this->db->from(self::SPACE);
		$this->db->join(self::LOCAL, self::LOCAL . '.local_id=' . self::SPACE . '.local_id');
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id=' . self::SPACE . '.company_id');
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id=' . self::SPACE . '.company_id');
		$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->count_all_results();
	}
	
	function getRowSpace($where) {
		$this->db->select('*');
		$this->db->from(self::SPACE);
		$this->db->join(self::LOCAL, self::LOCAL . '.local_id=' . self::SPACE . '.local_id');
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id=' . self::SPACE . '.company_id');
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id=' . self::SPACE . '.company_id');
		$this->db->where($where);
		return $this->db->get()->row();
	}
}
