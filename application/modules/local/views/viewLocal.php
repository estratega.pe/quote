<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form cmxform form-horizontal">
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">EMPRESA</label>
                                    <div class="controls">
                                        {SELECT_COMPANY_ID}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Nombre local</label>
                                    <div class="controls">
                                        {INPUT_LOCAL_NAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">E-mail</label>
                                    <div class="controls">
                                        {INPUT_LOCAL_EMAIL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Tel&eacute;fono</label>
                                    <div class="controls">
                                        {INPUT_LOCAL_PHONE}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="password" class="control-label">Direcci&oacute;n</label>
                                    <div class="controls">
                                        {TEXTAREA_LOCAL_ADDRESS}
                                    </div>
                                </div>

                                <div class="form-actions">
                                    {LINK_BACK}
                                </div>
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
			</div>