<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('home/homeModel', 'home');
		$this->load->model('company/companyModel', 'company');
		
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
		
	}
	
	public function index()
	{	
		$action = $this->uri->segment(3);
		
		$subtitle = 'LUGAR DE TRABAJO';
		$bg_color = 'blue';
		
		if($_POST) {
			switch ($action) {
				case 'setHome':
					if($this->UTYPE == $this->config->item('IND_ADMIN')) {
						$company_id = $this->input->post('select_company_id');
						$local_id = $this->input->post('select_local_id');
						if($this->gen->updateData('gen_user', ['company_id' => $company_id, 'local_id' => $local_id], ['user_id' => $this->UID]) === FALSE) {
							$subtitle = 'Error al guardar el LUGAR DE TRABAJO.';
							$bg_color = 'red';
						} else {
							$subtitle = 'LUGAR DE TRABAJO guardado con &eacute;xito.';
							$bg_color = 'green';
						}
					} else {
						$local_id = $this->input->post('select_local_id');
						if($this->gen->updateData('gen_user', ['local_id' => $local_id], ['user_id' => $this->UID]) === FALSE) {
							$subtitle = 'Error al guardar el LUGAR DE TRABAJO.';
							$bg_color = 'red';
						} else {
							$subtitle = 'LUGAR DE TRABAJO guardado con &eacute;xito.';
							$bg_color = 'green';
						}
					}
				break;
			}
		}
		
		
		$u = $this->gen->getRow('gen_user', ['user_id' => $this->UID]);
		
		$company_id = $u->company_id;
		$local_id = $u->local_id;
		$disabled_local =  (empty($u->local_id)) ? 'disabled' : '';
		
		if(empty($company_id) || empty($local_id) ) {
			$module = (empty($company_id)) ? 'Empresa' : 'Local';
			$module_link = (empty($company_id)) ? 'company/addCompany' : 'local/addLocal';
			if($u->user_type == $this->config->item('IND_ADMIN')) {
				$title = 'Cree ' . $module . ' primero';
				$subtitle = 'Cree ' . $module . ' primero';
				$bg_color = 'red';
				$link = createLink(base_url() . $module_link, ' ', 'icon-ban', 'Crear ' . $module, true);
			} else {
				$title = 'Solicite a su administrador que cree ' . $module . ' primero.';
				$subtitle = 'Solicite a su administrador que cree ' . $module . ' primero.';
				$bg_color = 'red';
				$link = '';
			}
			
			$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME Cree una empresa primero', 'BASE_URL' => base_url()];
			$data['BODY'] = $this->parser->parse('notice', ['BASE_URL' => base_url()
					,'BODY_TITLE'			=> $title
					,'BODY_SUBTITLE'		=> $subtitle
					,'BODY_MENU'			=> ''
					,'BG_COLOR'				=> $bg_color
					,'NOTICE'				=> $link
			], TRUE);
			renderPage($data);
			
		} else {
			$select_company_id = ['' => 'Seleccione'];
			$select_local_id = [];
			if($u->user_type == $this->config->item('IND_ADMIN')) {
				$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
				foreach($c as $k) {
					$select_company_id[$k->company_id] = $k->company_social;
				}
			} else {
				$select_company_id[$u->company_id] = $this->gen->getRow('jyc_company', ['company_id' => $u->company_id])->company_social;
			}
			$select_local_id = ['' => 'Seleccione'];
			
			
			if(!empty($company_id)) {
				$local = $this->gen->getData('jyc_local', ['company_id' => $company_id, 'local_status' => 1]);
				foreach($local as $l) {
					$select_local_id[$l->local_id] = $l->local_name;
				}
			}
			$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME ', 'BASE_URL' => base_url()];
			$data['BODY'] = $this->parser->parse('home/home_' . $this->UTYPE, ['BASE_URL' => base_url()
					,'BODY_TITLE'			=> 'Seleccionar lugar de trabajo'
					,'BODY_SUBTITLE'		=> $subtitle
					,'BODY_MENU'			=> ''
					,'BG_COLOR'				=> $bg_color
					,'URL_POST'				=> base_url() .'home/index/setHome/'
					,'SELECT_LOCAL_ID'		=> form_dropdown(array('name' => 'select_local_id', 'id' => 'select_local_id'), $select_local_id, $local_id, 'class="input-block-level" ' . $disabled_local . ' required')
					,'SELECT_COMPANY_ID'	=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="input-block-level" required')
					,'BUTTON_SUBMIT'		=> createSubmitButton('Guardar', 'btn-success', 'icon-save')
			], TRUE);
			$data['FOOTER_EXTRA'] = "
				<script>
				$('#select_company_id').change(function() {
					ID = $(this).val();
					if(ID >= 1) {
						$.ajax({
						  url: '" . base_url() . "space/getLocalOptions/' + ID,
						  dataType: 'html'
						}).done(function(row){
							$('#select_local_id').html(row).attr('disabled', false);
						  	$('#select_local_id').html(row).attr('required', true);
						});
					} else {
						$('#select_local_id').html('').attr('disabled', true);
					}
		
				});
				</script>
		";
			renderPage($data);
		}
	}		
		
}
